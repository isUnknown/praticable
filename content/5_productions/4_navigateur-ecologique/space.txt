Title: Navigateur web écologique

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"text":"### Le site du [cahier d'id\u00e9es pour un navigateur \u00e9cologique  \u2197](https:\/\/cahier.collectifbam.fr\/navecolo\/)","width":"585px","height":"124px","transform":"translate(45px, 90px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1664981357572","isHidden":false,"type":"markdown"},{"content":{"text":"Un cahier d'id\u00e9es pour une meilleure **ma\u00eetrise du web** et en faire un espace **sobre** et **lisible**.","width":"360px","height":"130px","transform":"translate(45px, 240px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1665239538018","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/navigateur-ecologique\/navigateur-ecologique-web-collectif-bam.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"420px","height":"4px","transform":"translate(465px, 165px)","zindex":0,"iscover":"true"},"id":"d8ef59c9-3f1d-4c30-8ae9-a5f3f8a12532","isHidden":false,"type":"image"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-10-05

----

Coverthumb: https://www.praticable.fr/media/pages/productions/navigateur-ecologique/29c3eec7c1-1664981330/navigateur-ecologique-web-collectif-bam.png

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris