Title: productions

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# productions","width":"540px","height":"125px","transform":"translate(45px, 90px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":false,"originalcontent":"# productions"},"id":"1662564162715","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Selon les situations, le d\u00e9faut d\u2019autonomie est plus ou moins probl\u00e9matique. Nos priorit\u00e9s sont les situations cruciales o\u00f9 l\u2019on se laisse faire non par choix mais par d\u00e9faut ou par manque de moyen de **se conduire de fa\u00e7on autonome**. Ainsi des pratiques num\u00e9riques et \u00e9cologiques, globalement incontr\u00f4lables par manque d\u2019outils, d\u2019options et d\u2019informations. En l\u2019occurrence, se laisser faire revient \u00e0 risquer nos libert\u00e9s et la persistance du monde vivant.","width":"690px","height":"241.5px","transform":"translate(75px, 210px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":true,"isedit":false,"originalcontent":"Selon les situations, le d\u00e9faut d\u2019autonomie est plus ou moins probl\u00e9matique. Nos priorit\u00e9s sont les situations cruciales o\u00f9 l\u2019on se laisse faire non par choix mais par d\u00e9faut ou par manque de moyen de **se conduire de fa\u00e7on autonome**. Ainsi des pratiques num\u00e9riques et \u00e9cologiques, globalement incontr\u00f4lables par manque d\u2019outils, d\u2019options et d\u2019informations. En l\u2019occurrence, se laisser faire revient \u00e0 risquer nos libert\u00e9s et la persistance du monde vivant."},"id":"1662564278355","isHidden":false,"type":"markdown"},{"content":{"title":"Limites num\u00e9riques","cover":"","intro":"Une **recherche en design** men\u00e9e aupr\u00e8s du laboratoire en informatique **LIRIS** du **CNRS** pour minimiser les impacts \u00e9cologiques du num\u00e9rique.","width":"540px","height":"189px","transform":"translate(30px, 510px)","zindex":0,"index":null,"url":"https:\/\/www.praticable.fr\/productions\/limites-numeriques","tags":"","linkedspaces":[]},"id":"productions\/limites-numeriques","isHidden":false,"type":"representative"},{"content":{"title":"Navigateur web \u00e9cologique","cover":"https:\/\/www.praticable.fr\/media\/pages\/productions\/navigateur-ecologique\/29c3eec7c1-1664981330\/navigateur-ecologique-web-collectif-bam.png","intro":"Un cahier d'id\u00e9es pour une meilleure **ma\u00eetrise du web** et en faire un espace **sobre** et **lisible**.","width":"390px","height":"229.5px","transform":"translate(540px, 570px)","zindex":0,"index":null,"url":"https:\/\/www.praticable.fr\/productions\/navigateur-ecologique","tags":"","linkedspaces":[]},"id":"productions\/navigateur-ecologique","isHidden":false,"type":"representative"},{"content":{"title":"Permis de (d\u00e9)construire","cover":"https:\/\/www.praticable.fr\/media\/pages\/productions\/permis-de-deconstruire\/6b1bb55554-1664543948\/praticable-plage-minetest-rendu.jpg","intro":"Une r\u00e9sidence pour imaginer **Aulnay-sous-Bois** avec des coll\u00e9gien\u00b7ne\u00b7s sur **Minetest** (version libre et open source de Minecraft).","width":"435px","height":"229.5px","transform":"translate(975px, 765px)","zindex":1,"index":null,"url":"https:\/\/www.praticable.fr\/productions\/permis-de-deconstruire","tags":"","linkedspaces":[]},"id":"productions\/permis-de-deconstruire","isHidden":false,"type":"representative"},{"content":{"isdesktoponly":"false","text":"- jhdkqfkdsqnf\n- jhdkqfkdsqnf\n- jhdkqfkdsqnf\n- jhdkqfkdsqnf\n- jhdkqfkdsqnf","width":"450px","height":"200px","transform":"translate(960px, 570px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":false,"originalcontent":"- jhdkqfkdsqnf\n- jhdkqfkdsqnf\n- jhdkqfkdsqnf\n- jhdkqfkdsqnf\n- jhdkqfkdsqnf"},"id":"1671587824978","isHidden":false,"type":"markdown"},{"content":{"title":"Station des savoirs","cover":"http:\/\/localhost:8888\/praticable\/media\/pages\/productions\/station-des-savoirs\/c584e73ea1-1671008625\/station-savoirs-praticable.jpg","intro":"Un projet r\u00e9alis\u00e9 pendant 4 jours au tiers-lieu paysan [**La Martini\u00e8re**](https:\/\/bluebees.fr\/fr\/project\/662-ferme-la-martiniere) pour \u00e9quiper localement la documentation des pratiques et savoirs de la ferme.","width":"555px","height":"189px","transform":"translate(1890px, 75px)","zindex":0,"index":null,"url":"http:\/\/localhost:8888\/praticable\/productions\/station-des-savoirs","tags":"thematique\/agriculture, types\/interface","linkedspaces":[]},"id":"productions\/station-des-savoirs","isHidden":false,"type":"representative"},{"content":{"title":"\u00c9tat des lieux du num\u00e9rique 2021","cover":"http:\/\/localhost:8888\/media\/pages\/productions\/etat-des-lieux-du-numerique-2021\/6735fbbf00-1672742767\/tmnlab_etude_site.png","intro":"Un outil pour pr\u00e9senter et **\u00e9tudier les \u00e9tudes** du TMNlab.","width":"450px","height":"201.25px","transform":"translate(1260px, 30px)","zindex":0,"index":null,"url":"http:\/\/localhost:8888\/productions\/etat-des-lieux-du-numerique-2021","tags":"","linkedspaces":["productions\/etat-des-lieux-du-numerique-2021\/sous-page-1","productions\/etat-des-lieux-du-numerique-2021\/sous-page-2"]},"id":"productions\/etat-des-lieux-du-numerique-2021","isHidden":false,"type":"representative"},{"content":{"title":"projet lien externe","cover":"","intro":"","width":"400px","height":"90.5px","transform":"translate(825px, 30px)","zindex":0,"index":null,"url":"https:\/\/praticable.fr\/navecolo\/","tags":"","linkedspaces":[]},"id":"productions\/projet-lien-externe","isHidden":false,"type":"representative"},{"content":{"title":"projet lien interne","cover":"","intro":"","width":"400px","height":"90.5px","transform":"translate(825px, 195px)","zindex":0,"index":null,"url":"http:\/\/localhost:8888\/productions\/projet-lien-interne","tags":"","linkedspaces":[]},"id":"productions\/projet-lien-interne","isHidden":false,"type":"representative"},{"content":{"text":"# test","width":"400px","height":"400px","transform":"translate(521px, 423.5px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":""},"id":"1676308129553","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-07-22

----

Linkedpages: 

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris

----

Uuid: D6xQsKpf6qjeVuxx