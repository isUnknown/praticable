Title: Station des savoirs

----

Isexternal: false

----

Externalurl: 

----

Tags: thematique/agriculture, types/interface

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"# station des savoirs","width":"510px","height":"202px","transform":"translate(30px, 90px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1660919997485","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Un projet r\u00e9alis\u00e9 pendant 4 jours au tiers-lieu paysan [**La Martini\u00e8re**](https:\/\/bluebees.fr\/fr\/project\/662-ferme-la-martiniere) pour \u00e9quiper localement la documentation des pratiques et savoirs de la ferme.","width":"540px","height":"133px","transform":"translate(585px, 90px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1665239279767","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### Documenter les pratiques et savoirs paysans d'une ferme\n\nLa station des savoirs est un mobilier mobile compos\u00e9 d\u2019\u00e9quipements num\u00e9riques r\u00e9utilis\u00e9s (ordinateur, disque dur\u2026) et de mat\u00e9riaux trouv\u00e9s sur place.","width":"510px","height":"266.5px","transform":"translate(30px, 345px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661157035793","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"#### La Martini\u00e8re\n\nLa Ferme de la Martini\u00e8re se voit comme un espace ouvert de transformation et d\u2019exp\u00e9rimentation autour de l\u2019autonomie alimentaire. Les contextes \u00e9cologiques, \u00e9conomiques et sociaux fragilisent et requestionnent le mod\u00e8le agricole. La Martini\u00e8re dessine une vision o\u00f9 les fermes de petites tailles, r\u00e9silientes et aux activit\u00e9s multiples sont les acteurs cl\u00e9s d\u2019une redynamisation des territoires.","width":"510px","height":"311.6px","transform":"translate(30px, 585px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661157075090","isHidden":false,"type":"markdown"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-praticable.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs - Praticable","caption":"","link":"","ratio":"","crop":"false","width":"765px","height":"4px","transform":"translate(585px, 240px)","zindex":0,"iscover":"true"},"id":"e5f995d0-430d-41e2-8e77-428a34f6c3d2","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-collectif-bam-light-4.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs ferme - Praticable","caption":"","link":"","ratio":"","crop":"false","width":"465px","height":"4px","transform":"translate(585px, 915px)","zindex":0,"iscover":"false"},"id":"132ff946-bdac-4318-8358-015a3ded37be","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-collectif-bam-light-2.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs ferme - Praticable","caption":"<p>La partie haute est destin\u00e9e \u00e0 des usages plus mobiles.<\/p>","link":"","ratio":"","crop":"false","width":"255px","height":"115px","transform":"translate(1095px, 915px)","zindex":0,"iscover":"false"},"id":"69460e9d-b995-45ee-ad0e-df678f6d7053","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-collectif-bam-light-3.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs ferme - Praticable","caption":"","link":"","ratio":"","crop":"false","width":"255px","height":"4px","transform":"translate(1095px, 1425px)","zindex":0,"iscover":"false"},"id":"4c0ee98e-8bd6-40b3-8132-5b354cf6cd82","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-collectif-bam-16.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs - Praticable","caption":"<p>La partie basse est destin\u00e9e au stockage de mat\u00e9riel plus lourd ou encombrant (enceintes, video-projecteur, rouleau \u00e9lectrique\u2026).<\/p>","link":"","ratio":"","crop":"false","width":"510px","height":"115px","transform":"translate(30px, 915px)","zindex":null,"iscover":"false"},"id":"e936a277-2664-44d9-b146-245f9852c771","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/guide-plan-collectif-bam-light.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs ferme - Praticable","caption":"<p>Extrait du guide de la station des savoirs.<\/p>","link":"","ratio":"","crop":"false","width":"465px","height":"73px","transform":"translate(585px, 1665px)","zindex":1,"iscover":"false"},"id":"688b5d92-88de-4270-a404-2834010101bc","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-collectif-bam-19.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs - Praticable","caption":"","link":"","ratio":"","crop":"false","width":"255px","height":"4px","transform":"translate(1095px, 1665px)","zindex":0,"iscover":"false"},"id":"b17fbcf6-c592-4e76-ba66-3ff82140dc83","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/station-savoirs-collectif-bam-9-light.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs ferme - Praticable","caption":"<p>Gr\u00e2ce \u00e0 l\u2019outil de documentation <a href=\"https:\/\/latelier-des-chercheurs.fr\/outils\/dodoc\"><strong>do\u2022doc<\/strong> \u2197<\/a> et au routeur install\u00e9, il est possible de documenter (prise de photo, vid\u00e9o, texte, audio\u2026) directement depuis un ou plusieurs smartphones sur une grande partie de la ferme sans avoir \u00e0 d\u00e9placer l\u2019ensemble.<\/p>","link":"","ratio":"","crop":"false","width":"510px","height":"157px","transform":"translate(30px, 1665px)","zindex":0,"iscover":"false"},"id":"2c4f3922-5401-43e6-9a12-7a49148b4551","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/interface-collectif-bam.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs ferme - Praticable","caption":"<p>Une interface a \u00e9t\u00e9 cr\u00e9\u00e9e pour permettre d\u2019acc\u00e9der \u00e0 l\u2019ensemble des savoirs pr\u00e9sents sur un disque dur.<\/p>","link":"","ratio":"","crop":"false","width":"510px","height":"94px","transform":"translate(30px, 2235px)","zindex":0,"iscover":"false"},"id":"f7c8bcc1-d08a-4b95-be8c-9f4a9423c912","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/schema-orga-light.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Stations des savoirs - Praticable","caption":"","link":"","ratio":"","crop":"false","width":"540px","height":"4px","transform":"translate(585px, 2235px)","zindex":0,"iscover":"false"},"id":"433c4333-ce76-4acf-a016-65e623149e9e","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/presentation.jpg"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station de savoirs - praticable","caption":"<p><a href=\"https:\/\/www.praticable.fr\/productions\/station-des-savoirs\/presentation.pdf\"><strong>Document de pr\u00e9sentation<\/strong> - PDF - 422,25&#160;KB \u2193<\/a><\/p>","link":"https:\/\/www.praticable.fr\/productions\/station-des-savoirs\/presentation.pdf","ratio":"","crop":"false","width":"510px","height":"73px","transform":"translate(30px, 2730px)","zindex":0,"iscover":"false"},"id":"6e9d89da-19a4-4e12-bc2a-c99000ffb10d","isHidden":false,"type":"image"},{"content":{"image":["productions\/station-des-savoirs\/guide.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"Station des savoirs - Praticable","caption":"<p><a href=\"https:\/\/www.praticable.fr\/productions\/station-des-savoirs\/guide.pdf\"><strong>Guide pour documenter sur la station<\/strong> - PDF - 827,54&#160;KB \u2193<\/a><\/p>","link":"","ratio":"","crop":"false","width":"510px","height":"94px","transform":"translate(585px, 2730px)","zindex":0,"iscover":"false"},"id":"78043f0a-eda8-46bb-8c25-bfe68a0b9a7f","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### Sp\u00e9cifications sur la partie num\u00e9rique\n\n- Ordinateur reconditionn\u00e9 sous Linux.\n\n- Installation de [**Do\u2022doc** \u2197](https:\/\/latelier-des-chercheurs.fr\/outils\/dodoc) pour documenter et cr\u00e9er des m\u00e9dia de documentations (stopmotion, diaporama, pdf etc).\n\n- Un routeur pour connecter plusieurs smartphones au do\u2022doc de l\u2019ordinateur via le r\u00e9seau Wifi et prendre directement photos, vid\u00e9os, audio n\u00e9cessaires \u00e0 la documentation.\n\n- Un disque dur interne reconditionn\u00e9 en externe pour sauvegarder automatiquement une copie des contenus de l\u2019ordinateur.\n\n- une m\u00e9diath\u00e8que de contenus sur le sujet (conf\u00e9rences, documentaires, tuto pdf et vid\u00e9o\u2026) class\u00e9s par cat\u00e9gories.","width":"1320px","height":"266.5px","transform":"translate(30px, 3225px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661159377908","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-08-19

----

Linkedpages: 

----

Coverthumb: http://localhost:8888/praticable/media/pages/productions/station-des-savoirs/c584e73ea1-1671008625/station-savoirs-praticable.jpg

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris