Title: espace

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"isdesktoponly":"false","text":"### didacticiels","width":"270px","height":"88px","transform":"translate(90px, 105px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666204665213","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"# espace","width":"405px","height":"142px","transform":"translate(90px, 210px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666204656291","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"Pourquoi des espaces, et non des pages ? Parce que ce site n'est pas \u00ab mis en page \u00bb, mais constitu\u00e9 d'**espaces librement praticables**. Pour aider \u00e0 leur prise en main, diff\u00e9rentes options possibles sont pr\u00e9sent\u00e9es dans ce didacticiel.","width":"660px","height":"156px","transform":"translate(90px, 375px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1666204932486","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"### se (d\u00e9)placer dans l'espace\n\nClic maintenu dans l'espace (hors des blocs) pour se d\u00e9placer.","width":"495px","height":"149px","transform":"translate(555px, 615px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666204845325","isHidden":false,"type":"markdown"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-se-deplacer.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"435px","height":"434.017px","transform":"translate(90px, 615px)","zindex":0,"iscover":"true"},"id":"c522fcc7-4320-4486-9179-7519f2a12bf5","isHidden":false,"type":"image"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-se-deplacer-grab.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"165px","transform":"translate(555px, 780px)","zindex":0,"iscover":"false"},"id":"4a21c995-2bb6-4a7c-92a0-0867c41d0423","isHidden":false,"type":"image"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-se-deplacer-grabbed.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"164.067px","transform":"translate(735px, 780px)","zindex":0,"iscover":"false"},"id":"65a53e91-031b-4937-9886-e2e8b8653f51","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### chercher un espace\n\nCliquer sur le bouton _chercher_ (en haut \u00e0 gauche de l'\u00e9cran), puis choisir un espace dans le menu. Pour revenir en arri\u00e8re, utiliser le bouton pr\u00e9c\u00e9dent du navigateur. Pour voir toutes les sous-espaces d'un espace, cliquer sur _tout_.","width":"540px","height":"201px","transform":"translate(555px, 1140px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666204834331","isHidden":false,"type":"markdown"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-chercher.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"420px","height":"419.183px","transform":"translate(105px, 1140px)","zindex":0,"iscover":"false"},"id":"bc5b3cd2-71f4-4b40-ad78-41d02a500f51","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### r\u00e9gler la composition\n\nCliquer sur le bouton _r\u00e9gler_ (en bas \u00e0 gauche de l'\u00e9cran), puis _composition_ et choisir un mode de pr\u00e9sentation. En mode diaporama, les blocs sont pr\u00e9sent\u00e9s un par un. Utiliser les fl\u00e8ches `\u2190` et `\u2192` pour d\u00e9filer. En mode _horizontale_, les blocs sont align\u00e9s \u00e0 l'horizontale. M\u00eame principe en mode _verticale_. Enfin, le mode _libre_ est une vue \u00e9clat\u00e9e des blocs.","width":"705px","height":"228.5px","transform":"translate(555px, 1650px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666204873043","isHidden":false,"type":"markdown"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-regler.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"420px","height":"420px","transform":"translate(105px, 1650px)","zindex":0,"iscover":"false"},"id":"325d385f-3742-403e-bb6e-2d09e02e3aba","isHidden":false,"type":"image"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-diaporama.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"165px","transform":"translate(1095px, 1905px)","zindex":0,"iscover":"false"},"id":"4424ee2d-c72c-4826-bd17-b6210d6b1905","isHidden":false,"type":"image"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-horizontal.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"164.067px","transform":"translate(915px, 1905px)","zindex":0,"iscover":"false"},"id":"68667be2-20a5-45bb-a711-307e096e1b19","isHidden":false,"type":"image"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-libre.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"164.067px","transform":"translate(555px, 1905px)","zindex":0,"iscover":"false"},"id":"52833aee-07b8-4d23-92f4-198f71ec474e","isHidden":false,"type":"image"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-vertical.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"165px","transform":"translate(735px, 1905px)","zindex":0,"iscover":"false"},"id":"bb21b2e8-65ec-4948-821d-6ac33eadb713","isHidden":false,"type":"image"},{"content":{"isdesktoponly":"false","text":"### r\u00e9initialiser un espace\n\nCliquer sur le bouton _\u00e9diter_ (en bas \u00e0 droite de l'\u00e9cran), puis sur _r\u00e9initialiser l'espace_. Cette fonction permet de restaurer la version originale d'un espace. Elle ne r\u00e9initialise pas tout le site. Pour effectuer cette derni\u00e8re, effacer les caches du navigateur.","width":"585px","height":"219px","transform":"translate(555px, 2160px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1666204887805","isHidden":false,"type":"markdown"},{"content":{"image":["didacticiels\/espace\/praticable-didactitiel-reinitialiser.png"],"src":"","isdesktoponly":"false","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"420px","height":"420px","transform":"translate(105px, 2160px)","zindex":0,"iscover":"false"},"id":"8089b4f9-3cbe-442a-a4c4-bdcb81931906","isHidden":false,"type":"image"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-10-19

----

Coverthumb: https://www.praticable.fr/media/pages/didacticiels/espace/a5e074c40f-1666347790/praticable-didactitiel-se-deplacer.png

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris