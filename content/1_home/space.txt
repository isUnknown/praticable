Title: accueil

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces:

- productions
- >
  studio/a-propos/vers-autre-chose-qu-une-marque
- studio/philosophie

----

Composition: [{"content":{"title":"productions","cover":"","intro":"Selon les situations, le d\u00e9faut d\u2019autonomie est plus ou moins probl\u00e9matique. Nos priorit\u00e9s sont les situations cruciales o\u00f9 l\u2019on se laisse faire non par choix mais par d\u00e9faut ou par manque de moyen de **se conduire de fa\u00e7on autonome**. Ainsi des pratiques num\u00e9riques et \u00e9cologiques, globalement incontr\u00f4lables par manque d\u2019outils, d\u2019options et d\u2019informations. En l\u2019occurrence, se laisser faire revient \u00e0 risquer nos libert\u00e9s et la persistance du monde vivant.","width":"420px","height":"405px","transform":"translate(555px, 510px)","zindex":1,"index":null,"url":"http:\/\/localhost:8888\/productions","tags":"","linkedspaces":[]},"id":"productions","isHidden":false,"type":"representative"},{"content":{"title":"philosophie","cover":"","intro":"L\u2019autonomie est en jeu dans les choses qui nous entourent. Elles peuvent nous **outiller**, nous **rendre capables**, et multiplier les **options possibles**.","width":"390px","height":"216px","transform":"translate(555px, 630px)","zindex":0,"index":null,"url":"http:\/\/localhost:8888\/studio\/philosophie","tags":"","linkedspaces":[]},"id":"studio\/philosophie","isHidden":false,"type":"representative"},{"content":{"isdesktoponly":"false","text":"[<i class=\"twitter\"><\/i>](https:\/\/twitter.com\/praticablecoop)[<i class=\"instagram\"><\/i>](https:\/\/www.instagram.com\/praticablecoop)[<i class=\"facebook\"><\/i>](https:\/\/www.facebook.com\/praticablecoop)[<i class=\"linkedin\"><\/i>](https:\/\/www.linkedin.com\/company\/praticablecoop)[<i class=\"mastodon\"><\/i>](https:\/\/mastodon.design\/web\/@praticablecoop)","width":"90px","height":"267.5px","transform":"translate(765px, 195px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":false,"originalcontent":"[<i class=\"twitter\"><\/i>](https:\/\/twitter.com\/praticablecoop)[<i class=\"instagram\"><\/i>](https:\/\/www.instagram.com\/praticablecoop)[<i class=\"facebook\"><\/i>](https:\/\/www.facebook.com\/praticablecoop)[<i class=\"linkedin\"><\/i>](https:\/\/www.linkedin.com\/company\/praticablecoop)[<i class=\"mastodon\"><\/i>](https:\/\/mastodon.design\/web\/@praticablecoop)"},"id":"1664549316351","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n- Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n\n- Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n\n- Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.\n\n**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n1. Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n2. Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n3. Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.\n\n**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n- Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n\n- Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n\n- Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.\n\n**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n- Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n\n- Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n\n- Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.","width":"504px","height":"1449.25px","transform":"translate(228px, 1512px)","zindex":9,"backgroundcolor":"#fff","textcolor":"#000","refs":"1661955796585","isintro":"false","isedit":false,"originalcontent":"**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n- Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n\n- Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n\n- Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.\n\n**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n1. Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n2. Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n3. Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.\n\n**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n- Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n\n- Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n\n- Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable.\n\n**Pouvoir faire soi-m\u00eame** a toujours \u00e9t\u00e9 la philosophie du studio. Aujourd\u2019hui, nous renfor\u00e7ons cette intention en nous concentrant sur les **enjeux num\u00e9riques**. Ce choix nous conduit naturellement \u00e0 effectuer quelques mises \u00e0 jour :\n\n- Nous avons chang\u00e9 notre nom pour le mot commun que nous utilisons le plus : **Praticable**.\n\n- Nous avons adopt\u00e9 une forme juridique plus adapt\u00e9e \u00e0 nos principes : une **coop\u00e9rative** \u00e0 mission.\n\n- Nous avons r\u00e9alis\u00e9 ce nouveau site internet \u00e9videmment praticable."},"id":"1659108799602","isHidden":false,"type":"markdown"},{"content":{"title":"vers autre chose qu'une marque","cover":"","intro":"","width":"405px","height":"131px","transform":"translate(750px, 795px)","zindex":0,"index":null,"url":"http:\/\/localhost:8888\/studio\/a-propos\/vers-autre-chose-qu-une-marque","tags":"","linkedspaces":[]},"id":"studio\/a-propos\/vers-autre-chose-qu-une-marque","isHidden":false,"type":"representative"},{"content":{"image":["praticable-didactitiel-camera.png"],"src":"","isdesktoponly":"true","location":"kirby","alt":"","caption":"<p>voici une avec un <a href=\"https:\/\/www.collectifbam.fr\">lien externe<\/a> et un <a href=\"http:\/\/localhost:8888\/studio\">lien interne<\/a> ici.<\/p>","link":"","ratio":"","crop":"false","width":"255px","height":"113px","transform":"translate(1188px, 1344px)","zindex":0,"iscover":"false"},"id":"626331d8-307e-47ab-bdca-1beccfd8e24b","isHidden":false,"type":"image"},{"content":{"image":["praticable-didactitiel-menu.png"],"src":"","isdesktoponly":"true","location":"kirby","alt":"","caption":"","link":"","ratio":"","crop":"false","width":"165px","height":"2px","transform":"translate(990px, 1350px)","zindex":13,"iscover":"false"},"id":"b65473cf-6702-422a-8fc9-bbbae4d892f3","isHidden":false,"type":"image"},{"content":{"code":"<input type=\"date\">\nqsdfffqsffq","width":"240px","height":"85px","transform":"translate(228px, 624px)","zindex":11,"backgroundcolor":"#FFF","textcolor":"#000","originalcontent":"<input type=\"date\">\nqsdfffqsffq","isedit":false},"id":"1668450977415","isHidden":false,"type":"code"},{"content":{"isdesktoponly":"false","text":"[lien interne](https:\/\/localhost:8888\/praticable\/studio\/)","width":"180px","height":"77.5px","transform":"translate(450px, 30px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":false,"originalcontent":"[lien interne](https:\/\/localhost:8888\/praticable\/studio\/)"},"id":"1672156062646","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"[lien externe](https:\/\/www.collectifbam.fr\/)","width":"180px","height":"77.5px","transform":"translate(675px, 30px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":false,"originalcontent":"[lien externe](https:\/\/www.collectifbam.fr\/)"},"id":"1672250925545","isHidden":false,"type":"markdown"},{"content":{"isdesktoponly":"false","text":"iframe:https:\/\/www.collectifbam.fr\/\n\nvoici du texte apr\u00e8s une iframe\n\niframe:https:\/\/www.collectifbam.fr\/","width":"732px","height":"1101px","transform":"translate(1092px, 96px)","zindex":8,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":false,"originalcontent":"iframe:https:\/\/www.collectifbam.fr\/\n\nvoici du texte apr\u00e8s une iframe\n\niframe:https:\/\/www.collectifbam.fr\/"},"id":"1672840986522","isHidden":false,"type":"markdown"},{"content":{"text":"# Titre\n> Citation","width":"400px","height":"196px","transform":"translate(1296px, 2112px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","originalcontent":"# Titre\n> Citation","isedit":false},"id":"1676283345311","isHidden":false,"type":"markdown"}]

----

Layout: free

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 167da87c132045bda1877e9b09088a38

----

Connections:

- 
  api: 'Notion - notion'
  id: e9454bf8cd034445bd81854c886d99e2
  target: subpages

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 

----

Linkedpages: - studio

----

Tag: 

----

Colors: jaune

----

Subpages: 

----

Iframe: <iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2F3pRXsofADkW5bwdYX4NH4n%2FDO-Plateforme-IQD---Maquettes%3Fpage-id%3D76%253A378%26node-id%3D76%253A379%26viewport%3D243%252C48%252C0.03%26scaling%3Dscale-down%26starting-point-node-id%3D76%253A379%26show-proto-sidebar%3D1" allowfullscreen></iframe>

----

Htmlfiles: - chaudron-test.zip

----

Pdf:

- >
  portfolio_margot-cannizzo-lazaro_2022.pdf

----

Coverthumb: 

----

Coverthumbtransform: 

----

Coverthumbzindex: 

----

Coverthumbwidth: 

----

Pagethumbtransform: 

----

Pagethumbzindex: 

----

Pagethumbwidth: 

----

Text: 

----

Uuid: AuHEvhiSEMxOZtER