Title: coopérative

----

Isexternal: false

----

Externalurl: 

----

Tags: 

----

Mode: composition

----

Linkedspaces: 

----

Composition: [{"content":{"text":"# \u00e9quipe","width":"345px","height":"142px","transform":"translate(435px, 210px)","zindex":32,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"3baff00b-4d3c-4438-adb9-f350717d873a","isHidden":false,"type":"markdown"},{"content":{"text":"**Laur\u00e8ne Zyskind**, designer","width":"210px","height":"104px","transform":"translate(450px, 45px)","zindex":23,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"aa9a6633-1fa8-456e-b2ee-7350bd7131bf","isHidden":false,"type":"markdown"},{"content":{"text":"**Adrien Payet**, philosophe et d\u00e9veloppeur","width":"195px","height":"130px","transform":"translate(855px, 195px)","zindex":31,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"04315081-1951-46b3-8d80-e493d99b537a","isHidden":false,"type":"markdown"},{"content":{"text":"**Morgane Chevalier**, designer et directrice g\u00e9n\u00e9rale","width":"240px","height":"130px","transform":"translate(120px, 210px)","zindex":25,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"aa84b306-734b-4157-af7c-dcf1d5cf2afb","isHidden":false,"type":"markdown"},{"content":{"text":"**Camille Adam**,\ndesigner","width":"210px","height":"104px","transform":"translate(270px, 330px)","zindex":26,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663248566306","isHidden":false,"type":"markdown"},{"content":{"text":"**Thomas Thibault**, designer et co-fondateur","width":"285px","height":"104px","transform":"translate(645px, 120px)","zindex":30,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"2c51aa84-1465-43fe-ad71-fb196f53666f","isHidden":false,"type":"markdown"},{"content":{"text":"**Alix Vignon**, designer","width":"165px","height":"104px","transform":"translate(735px, 315px)","zindex":33,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"7b143025-f479-49f0-b468-fa79fcf137d0","isHidden":false,"type":"markdown"},{"content":{"text":"**Anthony Ferretti**, designer, co-fondateur et pr\u00e9sident","width":"285px","height":"104px","transform":"translate(465px, 375px)","zindex":27,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"c6baa87c-8bb5-4284-8bb2-9eaedf1cc52e","isHidden":false,"type":"markdown"},{"content":{"text":"**L\u00e9a Grandin**, office manager","width":"225px","height":"104px","transform":"translate(255px, 120px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"cda5ddba-830f-460f-b39a-f2d907c92250","isHidden":false,"type":"markdown"},{"content":{"text":"## Une coop\u00e9rative ?","width":"525px","height":"106px","transform":"translate(45px, 570px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661169929904","isHidden":false,"type":"markdown"},{"content":{"text":"Collectif d\u2019ind\u00e9pendants en 2013, puis entreprise sous forme de [SAS \u2197\ufe0e](https:\/\/fr.wikipedia.org\/wiki\/Soci%C3%A9t%C3%A9_par_actions_simplifi%C3%A9e) (Soci\u00e9t\u00e9 par Actions Simplifi\u00e9e) en 2015, nous sommes devenus une **coop\u00e9rative \u00e0 mission** en mars 2022.","width":"495px","height":"156px","transform":"translate(60px, 660px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"true","isedit":"false"},"id":"1661169997922","isHidden":false,"type":"markdown"},{"content":{"text":"Ce choix constitue une adh\u00e9sion \u00e0 des valeurs coop\u00e9ratives fondamentales comme la pr\u00e9\u00e9minence de la personne **humaine**, la **d\u00e9mocratie** et le **partage**. La [**SCOP** \u2197](http:\/\/www.les-scop.coop\/les-scop) (Soci\u00e9t\u00e9 Coop\u00e9rative de Production) est un mod\u00e8le qui convient \u00e0 la **philosophie** du studio et \u00e0 son fonctionnement. Il se concr\u00e9tise entre autres par une gouvernance d\u00e9mocratique, des associ\u00e9s-salari\u00e9s majoritaires et un partage des b\u00e9n\u00e9fices.","width":"495px","height":"260px","transform":"translate(60px, 840px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1665254455164","isHidden":false,"type":"markdown"},{"content":{"text":"## \u00e0 mission ?","width":"525px","height":"106px","transform":"translate(630px, 570px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661171003049","isHidden":false,"type":"markdown"},{"content":{"text":"La qualit\u00e9 d'**entreprise \u00e0 mission** est encadr\u00e9e par la [**loi PACTE** \u2197](https:\/\/www.economie.gouv.fr\/cedef\/societe-mission). Elle nous permet de :\n- d\u00e9clarer dans nos statuts une **raison d'\u00eatre** et des **objectifs sociaux et environnementaux** associ\u00e9s,\n- de mettre en place un **comit\u00e9 de mission** charg\u00e9 de suivre et proposer des recommandations.\n\nCette sp\u00e9cificit\u00e9 est surtout pour nous l'occasion d\u2019am\u00e9nager **un espace r\u00e9flexif** plus officiel avec d'autres personnes sur la mani\u00e8re de pratiquer notre m\u00e9tier et favoriser l'autonomie au regard des enjeux contemporains.","width":"495px","height":"357.5px","transform":"translate(645px, 660px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661170057427","isHidden":false,"type":"markdown"},{"content":{"text":"### Raison d'\u00eatre\n\nFavoriser l\u2019**autonomie** en concevant des objets praticables permettant de **faire soi-m\u00eame**.","width":"495px","height":"149px","transform":"translate(645px, 1050px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661170156402","isHidden":false,"type":"markdown"},{"content":{"text":"### Objectifs sociaux et environnementaux\n\n- Concevoir des **objets praticables concrets** (produits, interfaces...) qui favorisent le d\u00e9veloppement de l\u2019autonomie dans les conditions sociales et environnementales de notre \u00e9poque.\n\n- Travailler dans des conditions **respectueuses du vivant et des droits humains**, avec des outils, des partenaires, des fournisseurs guid\u00e9s par les m\u00eames principes, et en m\u00e9c\u00e9nant des actions allant dans ce sens.\n\n- Contribuer aux **communs**, en partageant notre travail (objets, id\u00e9es, articles...) en licence **libre** ou toute autre action qui favorise la prise en main, la diffusion et l\u2019appropriation par le commun de nos productions ou celles d\u2019autrui.\n\n- **Prendre soin**, partager, participer \u00e0 la r\u00e9flexion, critiquer et faire \u00e9voluer la pratique de design et la n\u00f4tre, au regard des enjeux sociaux et environnementaux de notre \u00e9poque.","width":"495px","height":"601px","transform":"translate(645px, 1230px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249019853","isHidden":false,"type":"markdown"},{"content":{"text":"[**Statuts de l'entreprise** - PDF - 197,81 KB \u2193](https:\/\/www.praticable.fr\/media\/pages\/studio\/cooperative\/c73f8923bd-1660837251\/bam-scop-statuts.pdf)","width":"495px","height":"78px","transform":"translate(60px, 1125px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"01ad9ace-13be-4dfe-a359-c059081f57eb","isHidden":false,"type":"markdown"},{"content":{"text":"## comit\u00e9 de mission","width":"465px","height":"106px","transform":"translate(375px, 2070px)","zindex":36,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661169704673","isHidden":false,"type":"markdown"},{"content":{"text":"[**Pierre-Damien Huygue** \u2197\ufe0e](https:\/\/pierredamienhuyghe.fr\/1.html), philosophe","width":"285px","height":"104px","transform":"translate(705px, 2160px)","zindex":40,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249296378","isHidden":false,"type":"markdown"},{"content":{"text":"[**Nolwenn Maudet** \u2197\ufe0e](https:\/\/www.nolwennmaudet.com\/), enseignante-chercheuse","width":"300px","height":"104px","transform":"translate(885px, 2070px)","zindex":39,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249328112","isHidden":false,"type":"markdown"},{"content":{"text":"[**Legicoop** \u2197\ufe0e](https:\/\/legicoop.fr\/), Cabinet et coop\u00e9rative d'avocats","width":"240px","height":"104px","transform":"translate(60px, 2130px)","zindex":0,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249451755","isHidden":false,"type":"markdown"},{"content":{"text":"**Adrien Payet**, salari\u00e9","width":"180px","height":"104px","transform":"translate(795px, 1980px)","zindex":38,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249380565","isHidden":false,"type":"markdown"},{"content":{"text":"**Morgane Chevalier**, salari\u00e9e","width":"330px","height":"78px","transform":"translate(510px, 1920px)","zindex":34,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249266953","isHidden":false,"type":"markdown"},{"content":{"text":"**Alix Vignon**, salari\u00e9e","width":"225px","height":"78px","transform":"translate(525px, 2250px)","zindex":2,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249395249","isHidden":false,"type":"markdown"},{"content":{"text":"**Anthony Ferretti**, salari\u00e9","width":"270px","height":"78px","transform":"translate(285px, 2205px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1661171256768","isHidden":false,"type":"markdown"},{"content":{"text":"[**Charlotte Morel** \u2197\ufe0e](https:\/\/charlottemorel.fr\/), ancienne salari\u00e9e","width":"225px","height":"104px","transform":"translate(300px, 1950px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249417865","isHidden":false,"type":"markdown"},{"content":{"text":"**Thomas Thibault**, salari\u00e9","width":"210px","height":"104px","transform":"translate(180px, 2040px)","zindex":1,"backgroundcolor":"#fff","textcolor":"#000","refs":"","isintro":"false","isedit":"false"},"id":"1663249285774","isHidden":false,"type":"markdown"}]

----

Layout: 

----

Disablefreelayout: false

----

Isopenspace: false

----

Disablerepresentative: false

----

Isdesktoponly: false

----

Distantid: 

----

Connections: 

----

Seotitle: 

----

Description: 

----

Author: 

----

Published: 2022-07-19

----

Linkedpages: 

----

Coverthumb: 

----

Coverthumbtransform: 

----

Pagethumbtransform: 

----

Colors: gris