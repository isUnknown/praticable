const Store = {
  state: {
    isMobile: window.innerWidth < 850,
    isDesktop: window.innerWidth > 850,
    debug: true,
    blocks: {
      localVersion: [],
    },
    design: {
      blocks: {
        markdown: {
          width: 500,
          height: 400,
        },
        code: {
          width: 500,
          height: 400,
        },
        image: {
          width: 450,
          height: 450,
        },
      },
    },
    layout: "",
    originalLayout: "vertical",
    filters: [],
    slideIndex: 0,
    isUpToDate: true,
    saving: false,
    page: "",
    viewfinder: {
      isActive: false,
      currentSection: "",
      currentMenu: "",
    },
    contextMenu: {
      target: {},
      options: [
        // {
        //   label: '↑',
        //   function: 'goUpperBlock',
        //   concernedBlocks: ['text', 'image', 'representative'],
        //   condition: function(target) {
        //     return true
        //   }
        // },
        // {
        //   label: '↓',
        //   function: 'goLowerBlock',
        //   concernedBlocks: ['text', 'image', 'representative'],
        //   condition: function(target) {
        //     return true
        //   }
        // },
        {
          label: "supprimer",
          function: "removeBlockById",
          concernedBlocks: ["markdown", "code", "image"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "dupliquer",
          function: "duplicateBlockById",
          concernedBlocks: ["markdown", "code", "image"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "éditer",
          function: "editBlockById",
          concernedBlocks: ["markdown", "code"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "placer devant",
          function: "upper",
          concernedBlocks: ["image", "markdown", "code", "representative"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "placer au premier plan",
          function: "maxUpper",
          concernedBlocks: ["image", "markdown", "code", "representative"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "placer derrière",
          function: "lower",
          concernedBlocks: ["image", "markdown", "code", "representative"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "placer au dernier plan",
          function: "maxLower",
          concernedBlocks: ["image", "markdown", "code", "representative"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "définir comme couverture",
          function: "setAsCoverById",
          concernedBlocks: ["image"],
          condition: (target) => {
            return (
              Store.state.isLog &&
              (target.content.iscover == "false" ||
                target.content.iscover == false)
            );
          },
        },
        {
          label: "révoquer comme couverture",
          function: "unsetAsCoverById",
          concernedBlocks: ["image"],
          condition: (target) => {
            return (
              Store.state.isLog &&
              (target.content.iscover == "true" ||
                target.content.iscover == true)
            );
          },
        },
        {
          label: "définir comme introduction",
          function: "setAsIntroById",
          concernedBlocks: ["markdown"],
          condition: (target) => {
            return (
              Store.state.isLog &&
              (target.content.isintro == "false" ||
                target.content.isintro == false)
            );
          },
        },
        {
          label: "révoquer comme introduction",
          function: "unsetAsIntroById",
          concernedBlocks: ["markdown"],
          condition: (target) => {
            return (
              Store.state.isLog &&
              (target.content.isintro == "true" ||
                target.content.isintro == true)
            );
          },
        },
        {
          label: "supprimer tous les blocs",
          function: "removeAllBlocks",
          concernedBlocks: ["viewfinder"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "réinitialiser l'espace",
          function: "reset",
          concernedBlocks: ["viewfinder"],
          condition: function (target) {
            return true;
          },
        },
        {
          label: "supprimer les blocs sélectionnés",
          function: "removeSelectedBlocks",
          concernedBlocks: ["selection"],
          condition: function (target) {
            return true;
          },
        },
      ],
    },
  },
  storeVariables: function (variables) {
    variables.forEach((variable) => {
      this.state[variable.key] = variable.value;
    });
  },
  duplicateBlockById: function (id) {
    const copy = JSON.parse(JSON.stringify(this.getBlockById(id)));
    copy.id = String(Date.now());
    copy.isNew = true;

    const newX = parseInt(copy.content.transform.match(/[0-9]+/)[0]) + 15;
    const newY = parseInt(copy.content.transform.match(/[0-9]+/g)[1]) + 15;

    copy.content.transform = copy.content.transform.replace(/[0-9]+/g, newX);
    let t = 0;
    copy.content.transform = copy.content.transform.replace(
      /[0-9]+/g,
      (match) => (++t === 2 ? newY : match)
    );
    this.state.blocks.localVersion.push(copy);
    this.backupBlocks();
  },
  goUpperBlock: function (id) {
    const target = this.getBlockById(id);
    const index = this.getBlockIndex(id);
    if (index === 0) return;

    this.removeBlockById(id);
    const beforeTarget = this.state.blocks.localVersion.slice(0, index - 1);
    const afterTarget = this.state.blocks.localVersion.slice(
      index - 1,
      this.state.blocks.localVersion.length
    );
    this.state.blocks.localVersion = beforeTarget.concat(target, afterTarget);

    this.backupBlocks();
  },
  goLowerBlock: function (id) {
    const target = this.getBlockById(id);
    const index = this.getBlockIndex(id);
    if (index === this.state.blocks.localVersion.length - 1) return;

    this.removeBlockById(id);
    const beforeTarget = this.state.blocks.localVersion.slice(0, index + 1);
    const afterTarget = this.state.blocks.localVersion.slice(
      index + 1,
      this.state.blocks.localVersion.length
    );
    this.state.blocks.localVersion = beforeTarget.concat(target, afterTarget);

    this.backupBlocks();
  },
  getBlockIndex: function (id) {
    const index = this.state.blocks.localVersion.indexOf(this.getBlockById(id));
    return index;
  },
  storePageName: function (pageName) {
    this.state.page = pageName;
  },
  backupBlocks: function (showRegisterBtn = true) {
    let backups = JSON.parse(localStorage.getItem("backups"));
    if (backups[this.state.page].length === 20) {
      backups[this.state.page].pop();
    }
    backups[this.state.page].push(this.state.blocks.localVersion);
    localStorage.setItem("backups", JSON.stringify(backups));
    if (showRegisterBtn) {
      this.state.isUpToDate = false;
    }
  },
  isCurrentlyEditingBlock: function () {
    return this.state.blocks.localVersion.some(
      (block) => block.content.isedit == "true" || block.content.isedit == true
    );
  },
  undo: function () {
    // if (this.isCurrentlyEditingBlock()) return
    // let allBackups = JSON.parse(localStorage.getItem('backups'))
    // if (allBackups[this.state.page].length === 1) return
    // this.state.blocks.localVersion = allBackups[this.state.page][allBackups[this.state.page].length - 2]
    // if (allBackups[this.state.page].length > 1) {
    //   allBackups[this.state.page].pop()
    // }
    // localStorage.setItem('backups', JSON.stringify(allBackups))
    // this.state.isUpToDate = false
  },
  toggleFilter(tag) {
    if (tag.isActive) {
      tag.breadcrumb.forEach((crumb) => {
        this.state.filters.push(crumb);
      });
    } else {
      tag.breadcrumb.forEach((crumb) => {
        this.state.filters.splice(this.state.filters.indexOf(crumb), 1);
      });
    }
  },
  handleContextMenuFunction: function (name, blocks) {
    this[name](this.state.contextMenu.target.id, blocks);
  },
  setAsCoverById: function (id) {
    let oldCover = this.state.blocks.localVersion.find(
      (block) =>
        block.content.iscover == "true" || block.content.iscover == true
    );
    if (oldCover) {
      oldCover.content.iscover = false;
      // this.removeBlockById(oldCover.id)
      // this.state.blocks.localVersion.push(oldCover)
    }

    let newCover = this.state.blocks.localVersion.find(
      (block) => block.id === id
    );
    newCover.content.iscover = true;

    // this.removeBlockById(id)
    // this.state.blocks.localVersion.push(newCover)

    this.backupBlocks();
  },
  unsetAsCoverById: function (id) {
    let target = this.state.blocks.localVersion.find(
      (block) =>
        block.content.iscover == "true" || block.content.iscover == true
    );
    target.content.iscover = false;

    this.removeBlockById(target.id);
    this.state.blocks.localVersion.push(target);

    this.backupBlocks();
  },
  setAsIntroById: function (id) {
    let oldIntro = this.state.blocks.localVersion.find(
      (block) =>
        block.content.isintro == "true" || block.content.isintro == true
    );
    if (oldIntro) {
      oldIntro.content.isintro = false;
      // this.removeBlockById(oldIntro.id)
      // this.state.blocks.localVersion.push(oldIntro)
    }

    let newIntro = this.state.blocks.localVersion.find(
      (block) => block.id === id
    );
    newIntro.content.isintro = true;

    // this.removeBlockById(id)
    // this.state.blocks.localVersion.push(newIntro)

    this.backupBlocks();
  },
  unsetAsIntroById: function (id) {
    let target = this.state.blocks.localVersion.find(
      (block) =>
        block.content.isintro == "true" || block.content.isintro == true
    );
    target.content.isintro = false;

    // this.removeBlockById(target.id)
    // this.state.blocks.localVersion.push(target)

    this.backupBlocks();
  },
  editBlockById: function (id) {
    this.state.blocks.localVersion = this.state.blocks.localVersion.map(
      (block) => {
        if (block.id === id) {
          block.content.isedit = true;
        }
        return block;
      }
    );

    setTimeout(() => {
      const target = document.querySelectorAll("textarea")[1];
      if (target) {
        // this.scrollToBlock(id);
        target.focus();
      }
    }, 100);
  },
  scrollToBlock: function (id) {
    const block = this.getBlockById(id);

    const position = this.getPositionFromTransform(block.content.transform);
    const app = document.querySelector("#app");

    const appWidth = parseInt(app.style.width);
    const appHeight = parseInt(app.style.height);

    if (appWidth < window.innerWidth * 2) {
      app.classList.add("extend-width");
    }
    if (appWidth < window.innerHeight * 2) {
      app.classList.add("extend-height");
    }

    window.scrollTo(position.x - 100, position.y - 100);
  },
  editBlockPropertyById: function (id, property, value) {
    this.state.blocks.localVersion = this.state.blocks.localVersion.map(
      (block) => {
        if (block.id === id) {
          block.content[property] = value;
        }
        return block;
      }
    );
  },
  toggleEditBlockById: function (id) {
    const target = this.getBlockById(id);
    target.content.isedit =
      target.content.isedit === "true" || target.content.isedit === true
        ? false
        : true;
    console.log(target.content.isedit);
    setTimeout(() => {
      console.log(target.content.isedit);
    }, 500);
  },
  disableAllEdit: function () {
    const isEditBlock = this.state.blocks.localVersion.find(
      (block) =>
        block.content.isedit === true || block.content.isedit === "true"
    );
    if (!isEditBlock) return;

    this.state.blocks.localVersion = this.state.blocks.localVersion.map(
      (block) => {
        block.content.isedit = false;
        return block;
      }
    );
    if (document.querySelector("#app").classList.contains("extend")) {
      this.addTempTransitionToNode("#app");
      document.querySelector("#app").classList.remove("extend");
    }
    this.backupBlocks();
  },
  isEditingBlock: function () {
    return this.state.blocks.localVersion.some(
      (block) => block.content.isedit == true
    );
  },
  addTempTransitionToNode: function (
    selector,
    property = "all",
    duration = 0.5
  ) {
    const node = document.querySelector(selector);
    node.style.setProperty(
      "transition",
      `${property} ${duration}s ease-in-out`
    );
    node.style.setProperty(
      "-webkit-transition",
      `${property} ${duration}s ease-in-out`
    );
    node.style.setProperty(
      "-moz-transition",
      `${property} ${duration}s ease-in-out`
    );
    node.style.setProperty(
      "-o-transition",
      `${property} ${duration}s ease-in-out`
    );
    setTimeout(() => {
      node.style.setProperty("transition");
      node.style.setProperty("-webkit-transition");
      node.style.setProperty("-moz-transition");
      node.style.setProperty("-o-transition");
    }, duration * 1100);
  },
  unselectBlockById: function (id) {
    console.log("unselect");
    const block = this.getBlockById(id);
    block.isSelected = false;
  },
  clearSelection: function () {
    if (!document.querySelector("#selection")) return;
    this.updateBlocksPositionsFromSelection().then(() => {
      this.unselectBlocks();
    });
    document.querySelector("#selection").style.transform = "";
  },
  unselectBlocks: function () {
    this.state.blocks.localVersion = this.state.blocks.localVersion.map(
      (block) => {
        block.isSelected = false;
        if (block.type === "representative") block.isHidden = false;
        return block;
      }
    );
  },
  focusTextarea: function () {
    const textarea = document.querySelector("textarea");
    if (textarea) {
      textarea.focus();
    }
  },
  getPositionFromTransform: function (transform) {
    if (transform.length === 0)
      return {
        x: 0,
        y: 0,
      };
    const position = [...transform.matchAll(/-?\d+(\.\d+)?/g)];
    return {
      x: parseFloat(position[0]),
      y: parseFloat(position[1] || 0),
    };
  },
  removeSelectedBlocks: function () {
    if (this.state.isLog) {
      this.state.blocks.localVersion = this.state.blocks.localVersion.filter(
        (block) => {
          if (block.type === "representative" && block.isSelected) {
            block.isFlashing = true;
            setTimeout(() => {
              block.isFlashing = false;
            }, 1000);
          }

          return block.type === "representative" || !block.isSelected;
        }
      );
    } else {
      this.state.blocks.localVersion = this.state.blocks.localVersion.map(
        (block) => {
          if (!block.isSelected) return block;

          if (block.type === "representative") {
            block.isFlashing = true;
            setTimeout(() => {
              block.isFlashing = false;
            }, 1000);
          } else {
            block.isHidden = true;
          }

          return block;
        }
      );
      this.clearSelection();
    }
    this.backupBlocks();
  },
  updateBlocksPositionsFromSelection: function () {
    return new Promise((resolve) => {
      const selection = document.querySelector("#selection");
      if (!selection || selection.style.transform == "")
        resolve("No selection transformed");

      const selectionPosition = this.getPositionFromTransform(
        document.querySelector("#selection").style.transform
      );

      this.state.blocks.localVersion.forEach((block, index, blocks) => {
        if (block.isSelected) {
          const originalBlockPosition = this.getPositionFromTransform(
            block.content.transform
          );
          const newBlockPosition = {
            x: originalBlockPosition.x + selectionPosition.x,
            y: originalBlockPosition.y + selectionPosition.y,
          };
          block.content.transform = `translate(${newBlockPosition.x}px, ${newBlockPosition.y}px)`;
        }
        if (index === blocks.length - 1) {
          resolve("Positions updated");
        }
      });
    });
  },
  upper: function (id) {
    let target = this.state.blocks.localVersion.find(
      (block) => block.id === id
    );
    target.content.zindex++;

    // this.removeBlockById(id)
    // this.state.blocks.localVersion.push(target)

    this.backupBlocks();
  },
  maxUpper: function (id) {
    let highestBlock = undefined;
    this.state.blocks.localVersion.forEach((block) => {
      if (highestBlock === undefined) {
        highestBlock = block;
      } else {
        if (block.content.zindex > highestBlock.content.zindex) {
          highestBlock = block;
        }
      }
    });

    let target = this.state.blocks.localVersion.find(
      (block) => block.id === id
    );
    target.content.zindex = highestBlock.content.zindex + 1;

    // this.removeBlockById(id)
    // this.state.blocks.localVersion.push(target)

    this.backupBlocks();
  },
  lower: function (id) {
    let target = this.state.blocks.localVersion.find(
      (block) => block.id === id
    );
    if (target.content.zindex > 0) {
      target.content.zindex--;
    }

    // this.removeBlockById(id)
    // this.state.blocks.localVersion.push(target)

    this.backupBlocks();
  },
  maxLower: function (id) {
    let lowestBlock = undefined;
    this.state.blocks.localVersion.forEach((block) => {
      if (lowestBlock === undefined) {
        lowestBlock = block;
      } else {
        if (block.content.zindex < lowestBlock.content.zindex) {
          lowestBlock = block;
        }
      }
    });

    let target = this.state.blocks.localVersion.find(
      (block) => block.id === id
    );
    if (lowestBlock.zindex > 0) {
      target.content.zindex = lowestBlock.content.zindex - 1;
    } else {
      target.content.zindex = 0;
    }

    // this.removeBlockById(id)
    // this.state.blocks.localVersion.push(target)

    this.backupBlocks();
  },
  setLayout: function (layout) {
    this.state.layout = window.sessionStorage.getItem("layout") || layout;
    window.sessionStorage.setItem("layout", this.state.layout);
  },
  setOriginalLayout: function (layout) {
    this.state.originalLayout = layout;
  },
  changeLayout: function (layout) {
    this.state.layout = layout;
    window.sessionStorage.setItem("layout", this.state.layout);
  },
  changeBlockColor: function (id, mode, color) {
    this.state.blocks.localVersion = this.state.blocks.localVersion.map(
      (block) => {
        if (block.id !== id) return block;
        if (mode === "markdown") block.content.textcolor = color;
        if (mode === "background") block.content.backgroundcolor = color;
        return block;
      }
    );
  },
  setServerBlocks: function (originalBlocks) {
    this.state.blocks.serverVersion = originalBlocks;
  },
  setBlocks: function (blocks) {
    blocks.forEach((block) => {
      block.isHighlight = false;
      block.isVisible = true;
      block.isSelected = false;
      block.isFlashing = false;
      block.content.isedit = false;

      if (block.content.originalContent || block.type === "representative")
        return;
      if (block.type === "markdown")
        block.content.originalContent = block.content.text;
      if (block.type === "code")
        block.content.originalContent = block.content.code;
      if (block.type === "image")
        block.content.originalContent = block.content.image[0];
    });

    this.state.blocks.localVersion = blocks;
  },
  addToGroup: function (blockId, groupId) {
    const blockToAdd = this.getBlockById(blockId);
    this.removeBlockById(blockId);
    this.state.blocks.localVersion.forEach((block) => {
      if (block.id === groupId) {
        block.content.blocks.push(blockToAdd);
      }
    });
  },
  getBlockByTitle: function (title) {
    return this.state.blocks.localVersion.find(
      (block) => block.content.title === title
    );
  },
  getBlockById: function (id) {
    return this.state.blocks.localVersion.find((block) => block.id === id);
  },
  highlightBlockByTitle: function (title) {
    this.state.blocks.localVersion.forEach((block) => {
      if (block.content.title === title) {
        block.isHighlight = true;
      }
    });
  },
  unhighlightBlockByTitle: function (title) {
    this.state.blocks.localVersion.forEach((block) => {
      if (block.content.title === title) {
        block.isHighlight = false;
      }
    });
  },
  highlightBlockById: function (id) {
    this.state.blocks.localVersion.forEach((block) => {
      if (block.id == id) {
        block.isHighlight = true;
      }
    });
  },
  unhighlightBlockById: function (id) {
    this.state.blocks.localVersion.forEach((block) => {
      if (block.id == id) {
        block.isHighlight = false;
      }
    });
  },
  removeAllBlocks: function () {
    this.state.blocks.localVersion = {};
    this.backupBlocks();
  },
  removeBlockById: function (id) {
    const targetBlock = this.getBlockById(id);

    if (targetBlock.type === "representative") return;

    if (this.state.isLog || targetBlock.isLocal) {
      this.state.blocks.localVersion = this.state.blocks.localVersion.filter(
        (block) => block.id != id
      );
    } else {
      targetBlock.isHidden = true;
    }
    this.backupBlocks();
  },
  replaceBlockById: function (idToReplace, newBlock) {
    const targetBlock = this.getBlockById(idToReplace);
    targetBlock.isHidden = true;
    this.state.blocks.localVersion = this.state.blocks.localVersion.filter(
      (block) => block.id != idToReplace
    );
    this.state.blocks.localVersion.push(newBlock);
    this.backupBlocks();
  },
  removeNewBlock: function () {
    this.state.blocks.localVersion = this.state.blocks.localVersion.filter(
      (block) => !block.isNew
    );
  },
  removeCover: function () {
    this.state.blocks.localVersion.forEach((block) => {
      if (block.type === "image") {
        block.content.iscover = "false";
      }
    });
  },
  addBlock: function ({
    type,
    id = Date.now(),
    refs = "",
    left = "0",
    top = "0",
    text = "",
  }) {
    this.backupBlocks();
    let newBlock = {
      type: type,
      isNew: true,
      isEdited: false,
      isHighlight: false,
      isLocal: true,
      id: id,
      content: {
        width: this.state.design.blocks[type].width + "px",
        height: this.state.design.blocks[type].width + "px",
        transform: `translate(${left}px, ${top}px)`,
        zindex: 0,
        isedit: "true",
      },
    };

    if (type === "markdown") {
      newBlock.content.text = text;
      newBlock.content.refs = refs;
    }
    if (type === "code") {
      newBlock.content.code = text;
    }
    if (type === "image") {
      newBlock.content.caption = "";
    }

    this.state.blocks.localVersion.push(newBlock);
    this.focusField();
  },
  focusField: function () {
    setTimeout(() => {
      const field = document.querySelector("textarea");
      if (field) {
        field.focus();
      }
    }, 0);
  },
  reset: function () {
    console.log("reset");
    this.resetBlocks();
    this.resetLayout();
    this.state.isUpToDate = true;
    window.location.reload();
  },
  resetBlocks: function () {
    this.setBlocks(this.state.blocks.serverVersion);
    this.backupBlocks();
  },
  resetLayout: function () {
    this.state.layout = this.state.originalLayout;
    window.sessionStorage.setItem("layout", this.state.layout);
  },
  getLastBlock: function () {
    return this.state.blocks.localVersion[
      this.state.blocks.localVersion.length - 1
    ];
  },
  getRightMostBlock: function () {
    const blockNodesArray = Array.prototype.slice.call(
      document.querySelectorAll(".block")
    );
    const sortedBlockNodes = blockNodesArray.sort((blockA, blockB) => {
      const blockARightPosition = this.getBlockRightPosition(blockA);
      const blockBRightPosition = this.getBlockRightPosition(blockB);
      return blockBRightPosition - blockARightPosition;
    });
    const rightMostBlock = sortedBlockNodes[0];
    return rightMostBlock;
  },
  getBlockRightPosition: function (blockNode) {
    const left = this.getPositionFromTransform(blockNode.style.transform).x;
    const width = parseInt(
      window.getComputedStyle(blockNode).width.replace("px", "")
    );
    const right = left + width;
    return right;
  },
  getLowestBlock: function () {
    const blockNodesArray = Array.prototype.slice.call(
      document.querySelectorAll(".block")
    );
    const sortedBlockNodes = blockNodesArray.sort((blockA, blockB) => {
      const blockABottomPosition = this.getBlockBottomPosition(blockA);
      const blockBBottomPosition = this.getBlockBottomPosition(blockB);
      return blockBBottomPosition - blockABottomPosition;
    });
    const lowestBlock = sortedBlockNodes[0];
    return lowestBlock;
  },
  getBlockBottomPosition: function (blockNode) {
    const top = this.getPositionFromTransform(blockNode.style.transform).y;
    const height = parseInt(
      window.getComputedStyle(blockNode).height.replace("px", "")
    );
    const bottom = top + height;
    return bottom;
  },
  proceedServerUpdates: function () {
    this.state.blocks.serverVersion.forEach((serverBlock) => {
      const localBlock = this.state.blocks.localVersion.find(
        (localBlock) => localBlock.id === serverBlock.id
      );

      if (!localBlock) {
        this.state.blocks.localVersion.push(serverBlock);
      } else {
        this.replaceBlockIfDifferent(serverBlock, localBlock);
      }
    });

    if (
      this.state.blocks.localVersion.length >
      this.state.blocks.serverVersion.length
    ) {
      this.removeServerRemovedBlock();
    }
  },
  replaceBlockIfDifferent: function (serverBlock, localBlock) {
    const isRepresentative = serverBlock.type === "representative";
    const isText = serverBlock.type === "markdown";
    const isImage = serverBlock.type === "image";
    if (
      (isRepresentative &&
        (serverBlock.content.intro !== localBlock.content.intro ||
          serverBlock.content.cover !== localBlock.content.cover)) ||
      (isText &&
        serverBlock.content.text !== localBlock.content.originalContent) ||
      (isImage &&
        serverBlock.content.image[0] !== localBlock.content.originalContent)
    ) {
      if (serverBlock.type === "markdown") {
        console.log("serverBlock.content.text", serverBlock.content.text);
        console.log(
          "localBlock.content.originalContent",
          localBlock.content.originalContent
        );
      }
      serverBlock.content.originalContent = serverBlock.content.text;
      this.replaceBlockById(localBlock.id, serverBlock);
    }
  },
  removeServerRemovedBlock: function () {
    this.state.blocks.localVersion.forEach((localBlock) => {
      if (localBlock.isLocal) return;
      if (
        !this.state.blocks.serverVersion.some(
          (serverBlock) => serverBlock.id === localBlock.id
        )
      )
        this.removeBlockById(localBlock.id);
    });
  },
  convertBlockTextToMarkdown: function () {
    this.state.blocks.localVersion = this.state.blocks.localVersion.map(
      (block) => {
        if (block.type === "text") {
          block.type === "markdown";
        }
        return block;
      }
    );
    this.state.blocks.serverVersion = this.state.blocks.serverVersion.map(
      (block) => {
        if (block.type === "text") {
          block.type === "markdown";
        }
        return block;
      }
    );

    this.backupBlocks(false);
  },
};

export default Store;
