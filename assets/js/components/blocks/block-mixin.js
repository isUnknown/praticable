import Store from "../../store.js";
import contextMenuMixin from "../viewfinder/mixins/context-menu-mixin.js";

const blockMixin = {
  props: {
    block: Object,
    updateSignal: Number,
    page: String,
  },
  mixins: [contextMenuMixin],
  data: function () {
    return {
      store: Store,
    };
  },
  methods: {
    slugify: function (str) {
      str = str.replace(/^\s+|\s+$/g, "");

      // Make the string lowercase
      str = str.toLowerCase();

      // Remove invalid chars except last dot (.extension)
      const last = str.lastIndexOf(".");
      const butLast = str.substring(0, last).replace(/\./g, "");
      str = butLast + str.substring(last);

      // Remove accents, swap ñ for n, etc
      const from = "áäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
      const to = "aaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
      for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
      }

      // Collapse whitespace and replace by -
      str = str
        .replace(/\s+/g, "-")

        // Collapse dashes
        .replace(/-+/g, "-");

      return str;
    },
    click: function (e) {
      if (e.target.closest("a")) return;
      const isSelectedText = window.getSelection().toString().length > 0;
      const hasBeenMoved =
        this.block.content.transform !== this.$refs.block.style.transform;
      if (isSelectedText || hasBeenMoved) return;
      this.select(e);
      if (e.altKey) {
        this.store.duplicateBlockById(this.block.id);
      }
      this.saveStyle();
    },
    select: function (e) {
      if (this.store.state.isMobile) return;
      if (
        e.target.closest(".prevent-unselect") ||
        this.block.type === "representative" ||
        e.metaKey ||
        e.ctrlKey ||
        e.target.tagName === "INPUT"
      ) {
        return;
      }
      const originalState = this.block.isSelected;
      if (!e.ctrlKey && !e.metaKey) this.store.unselectBlocks();
      this.block.isSelected = originalState === true ? false : true;
    },
    saveStyle: function () {
      const saveStyle = () => {
        if (this.$root.isEditMode) {
          if (!this.$refs.block) return;
          const actualStyle = this.$refs.block.style;
          this.block.content.width = actualStyle.width;
          this.block.content.height = actualStyle.height;
          this.block.content.transform = actualStyle.transform;

          if (this.isHover === false) {
            this.block.content.zindex = parseInt(actualStyle.zIndex) || 0;
          }
        }
        document.removeEventListener("mouseup", saveStyle);
      };

      document.addEventListener("mouseup", saveStyle);
    },
  },
  mounted: function () {
    if (!this.$refs.block) return;

    this.block.content.height = window
      .getComputedStyle(this.$refs.block)
      .getPropertyValue("height");
  },
};

export default blockMixin;
