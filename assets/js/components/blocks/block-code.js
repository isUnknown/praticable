const BlockCode = {
  props: {
    block: Object,
    isEdit: Boolean,
  },
  template: `
        <div>
            <div v-if="block.content.code.length > 0" v-html="block.content.code" ref="content"></div>
            <div v-else ref="content"><p>Double-cliquez pour éditer</p></div>
        </div>
    `,
};

export default BlockCode;
