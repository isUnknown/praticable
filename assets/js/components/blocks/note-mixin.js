const noteMixin = {
  computed: {
    noteFirstIndex: function() {
      let noteFirstIndex = 0
      const regex = /\[([0-9+])\]/
      const firstIndex = this.block.content.text.match(regex)
      if (!firstIndex) return noteFirstIndex
      noteFirstIndex = parseInt(firstIndex.pop())
      
      return noteFirstIndex
    },
    noteIndex: function() {
      let noteIndex = 1
      const regex = /\[([0-9+])\]/gm
      const indexes = this.block.content.text.matchAll(regex)
      const array = Array.from(indexes)
      if (array.length === 0) return 1
      const lastIndexGroup = array.pop()
      const lastIndex = parseInt(lastIndexGroup.pop())
      noteIndex = lastIndex + 1
      
      return noteIndex
    }
  }
}

export default noteMixin