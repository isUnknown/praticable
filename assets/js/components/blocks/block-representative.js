import Store from "../../store.js";

const BlockRepresentative = {
  props: {
    block: Object,
  },
  data: function () {
    return {
      store: Store,
    };
  },
  computed: {
    iframeHeight: function () {
      return parseInt(this.block.content.width) / 1.5;
    },
    markedIntro: function () {
      if (this.block.content.intro.length === 0) return false;

      // const sum =  !this.block.content.intro.includes('iframe') && this.block.content.intro.length > 150 ? this.block.content.intro.slice(0, 150) + '…' : this.block.content.intro

      const regexYoutubeUrls =
        /^(?!\()(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?.*v=)?(\w+)/gm;
      const videoConvertedText = this.block.content.intro.replaceAll(
        regexYoutubeUrls,
        `<iframe width="100%" style="height:${this.iframeHeight}px" src="https://www.youtube-nocookie.com/embed/$1" frameborder="0" allowfullscreen></iframe>`
      );
      const iframeConverted = videoConvertedText.replaceAll(
        /iframe:(.*)/gm,
        `<iframe width="100%" style="height:${this.iframeHeight}px" src="$1"></iframe>`
      );

      marked.setOptions({
        breaks: true,
        smartypants: true,
      });
      let markedIntro = marked.parse(iframeConverted);
      return markedIntro;
    },
    tags: function () {
      if (this.block.content.tags.length === 0) return [];

      let tags = this.block.content.tags.replaceAll("/", "A"); // replace all / by A to merge crumbs into words
      tags = tags.replaceAll(/\b[a-z]+A?,?\s?/gm, ""); // remove categories
      tags = tags.replaceAll("A", "/"); // reset slashes
      tags = tags.split(/[, \/]+/); // split by commas

      const tagsWithoutDoubles = [...new Set(tags)];
      return tagsWithoutDoubles;
    },
  },
  template: `
    <div ref="block">
      <div v-if="block.content.cover.length > 0" class="block--representative__image">
        <figure>
          <img :src="block.content.cover" alt="">
        </figure>
      </div>
      <div class="block--representative__sum">
        <h3 class="block--representative__heading">
          <a 
            :href="block.content.url" 
            :title="'Aller à la page ' + block.content.title + '.'" 
            :target="setTarget(block.content.url)"
          >{{ block.content.title }}</a>
        </h3>
        <p v-if="markedIntro" class="block--representative_introduction" v-html="markedIntro"></p>
        <div v-if="tags.length > 0 || block.content.linkedspaces.length > 0" class="block--representative__data">
          <!-- <div v-if="tags.length > 0" class="block--representative__filters">
            <p v-if="tags.length > 0" class="text-4">filtres :</p>
            <button v-for="tag in tags"
              class="filter | text-4 border lock-active off"
            >{{ tag }}</button>
          </div> --> 
          <ul class="block--representative__list-linked-spaces">
            <!-- <p v-if="linkedSpaces.length > 0" class="block--representative__heading-linked-spaces">espaces liés :</p> -->
            <li v-for="linkedSpace in block.content.linkedspaces" class="block--representative__linked-spaces">
              <a :href="linkedSpace.url">{{ linkedSpace.title }}</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  `,
  methods: {
    setTarget: function(url) {
      if (url.includes(location.host)) {
        return false
      } else {
        return '_blank'
      }
    }
  }
};

export default BlockRepresentative;
