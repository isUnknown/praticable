import Store from "../../store.js";

const BtnsEdit = {
  props: ["block"],
  data: function () {
    return {
      store: Store,
      initialText:
        this.block.type === "text"
          ? this.block.content.text
          : this.block.content.code,
    };
  },
  computed: {
    isEditMode: function () {
      return (
        this.block.content.isedit === true ||
        this.block.content.isedit === "true"
      );
    },
    isMultiselect: function () {
      return (
        this.store.state.blocks.localVersion.filter((block) => block.isSelected)
          .length > 1
      );
    },
  },
  template: `
    <div 
      v-if="!isMultiselect"
      id="edit-buttons--left" 
      class="| flex prevent-unselect"
    >
        <button 
            v-if="isEditMode && block.type !== 'image'"
            id="valid" 
            class="| border border--blue prevent-unselect"
            title="Valider le bloc."
            @click="valid"
        >
          valider
          <span class="sr-only">valider</span>
        </button>
        <button 
            v-if="isEditMode && block.type !== 'image'"
            id="cancel" 
            class="| border border--blue prevent-unselect"
            title="Annuler l'action."
            @click="cancel"
        >
          annuler
          <span class="sr-only">annuler</span>
        </button>
        <button 
            v-if="!isEditMode && block.type !== 'image'"
            id="edit" 
            class="| border border--blue prevent-unselect"
            title="Éditer le bloc. Vos modifications sont privées."
            @click="edit"
        >
          éditer
          <span class="sr-only">éditer</span>
        </button>
        <button 
          v-if="block.type !== 'representative'"
          id="remove" 
          class="| border border--blue prevent-unselect"
          title="Supprimer le bloc."
          @click="remove"
        >
          supprimer
          <span class="sr-only">supprimer</span>
        </button>
    </div>
  `,
  methods: {
    valid: function () {
      this.block.content.isedit = false;
      this.store.backupBlocks();
    },
    remove: function () {
      if (this.block.content.isedit == true) return;
      this.store.removeBlockById(this.block.id);
    },
    cancel: function () {
      if (
        (this.block.type === "text" && this.block.content.text.length === 0) ||
        (this.block.type === "code" && this.block.content.code.length === 0)
      ) {
        this.remove();
      } else {
        if (this.block.type === "text") {
          this.block.content.code = this.initialText;
        } else {
          this.block.content.code = this.initialText;
        }
        this.block.content.isedit = false;
      }
    },
    edit: function () {
      this.store.editBlockById(this.block.id);
    },
    enableShortkeys: function () {
      document.addEventListener("keydown", (event) => {
        if (event.key === "Escape") {
          this.cancel();
        }
        if ((event.ctrlKey || event.metaKey) && event.key === "Enter") {
          this.valid();
        }
        if ((event.ctrlKey || event.metaKey) && event.key === "Backspace") {
          this.remove();
        }

        if (
          (this.block.isedit !== true || this.block.isedit !== "true") &&
          this.block.isSelected &&
          event.key === "Backspace"
        ) {
          this.remove();
        }
      });

      document.addEventListener("click", (event) => {
        if (
          event.target.textContent === "texte" ||
          event.target.textContent === "code" ||
          event.target.tagName === "TEXTAREA"
        )
          return;

        if (
          (this.block.type === "text" &&
            this.block.content.text.length === 0) ||
          (this.block.type === "code" && this.block.content.code.length === 0)
        ) {
          this.remove();
        } else if (
          event.target.nodeName === "HTML" ||
          event.target.id === "app" ||
          !event.target.closest(".prevent-unselect")
        ) {
          this.store.disableAllEdit();
        }
      });
    },
  },
  mounted: function () {
    this.enableShortkeys();
  },
};

export default BtnsEdit;
