import Store from "../../../../store.js";

const PrevNext = {
  data: function () {
    return {
      store: Store,
    };
  },
  template: `
    <div id="slide-nav" class="viewfinder__section viewfinder__btn">
      <button id="slide-nav__switch--prev" class="slide-nav__switch menu__arrow | border prevent-viewfinder-layer lock-active text-2" @click="prev"><-</button>
      <button id="slide-nav__switch--next" class="slide-nav__switch menu__arrow | border prevent-viewfinder-layer lock-active text-2" @click="next">-></button>
    </div>
  `,
  methods: {
    prev: function () {
      if (this.store.state.slideIndex === 0) {
        this.store.state.slideIndex =
          this.store.state.blocks.localVersion.length - 1;
      } else {
        this.store.state.slideIndex--;
      }
    },
    next: function () {
      if (
        this.store.state.slideIndex ===
        this.store.state.blocks.localVersion.length - 1
      ) {
        this.store.state.slideIndex = 0;
      } else {
        this.store.state.slideIndex++;
      }
    },
  },
};

export default PrevNext;
