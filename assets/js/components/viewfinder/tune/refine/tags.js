import Store from '../../../../store.js';

const Tags = {
  name: 'tags',
  props: {
    'tags': Array,
    'operatingFilters': Array
  },
  data: function () {
    return {
      'store': Store
    }
  },
  template: `
    <ul>     
      <li
        v-for="(tag, tagIndex) in tags"
        v-if="operatingFilters.includes(removeAccents(tag.title.toLowerCase()))"
      >
        <span>
          <button 
            class="text-2 border"
            :class="{ 'lock-active': store.state.filters.includes(removeAccents(tag.title.toLowerCase())) }"
            tabindex="-1"
            @click="toggleFilters(tag, $event)"
          >
            {{ tag.title }}
          </button>
          
          <span 
            v-if="tag.children"
            class="menu__arrow | text-2 border"
            tabindex="-1"
          >-></span>
        </span>
        <tags
          v-if="tag.children"
          :tags="tag.children"
          :operatingFilters="operatingFilters"
        ></tags>
      </li>
    </ul>
  `,
  methods: {
    removeAccents: function (str) {
      return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    },
    toggleFilters: function (tag, event) {
      tag.isActive = !tag.isActive
      this.store.toggleFilter(tag)
    }
  }
}

export default Tags