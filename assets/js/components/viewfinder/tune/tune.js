import Refine from "./refine/refine.js";
import Composition from "./composition/composition.js";
import viewfinderBtnMixin from "../mixins/viewfinder-btn-mixin.js";
import Store from "../../../store.js";
import Tuneviewfinder from "./viewfinder/tune-viewfinder.js";

const Tune = {
  props: {
    tags: Array,
  },
  mixins: [viewfinderBtnMixin],
  components: {
    refine: Refine,
    composition: Composition,
    "tune-viewfinder": Tuneviewfinder,
  },
  data: function () {
    return {
      isOpen: false,
      store: Store,
      name: "régler",
    };
  },
  template: `
    <ul class="menu bottom left">
      <li>
        <span @click="toggleIsOpen(name)" :class="{'hidden--mobile': store.state.viewfinder.currentSection.length > 0}">
          <button 
            class="| text-2 border lock-active"
            title="Régler l'affichage du site."
          >{{ this.name }}</button>
          <span
            class="menu__arrow | text-2 border lock-active"
            tabindex="0"
          >-></span>
        </span>
        <ul v-if="isOpen" class="lock-visible--flex">
          <refine :tags="tags"></refine>
          <composition @switchOffViewfinder="switchOffViewfinder"></composition>
          <!-- <tune-viewfinder></tune-viewfinder> -->
        </ul>
      </li>
    </ul>
  `,
  methods: {
    switchOffViewfinder: function () {
      this.$emit("switchOffViewfinder");
    },
  },
};

export default Tune;
