import Store from "../../store.js";

const Register = {
  props: {
    page: String,
    "page-colors-label": String,
  },
  data: function () {
    return {
      store: Store,
    };
  },
  template: `
    <button 
      class="| text-2 border"
      title="Enregistrer la page actuelle." 
      @click="save"
      ref="register-btn"
    >enregistrer</button>
  `,
  methods: {
    save: function () {
      if (!this.store.state.isLog) return;
      this.$refs["register-btn"].textContent = "enregistrement…";
      setTimeout(() => {
        const blocks = {
          page: this.page,
          colors: this.pageColorsLabel,
          blocks: this.store.state.blocks.localVersion,
        };
        if (this.$root.debug) {
          console.log(this.$options.el + " save: ", blocks);
        }
        this.send(blocks);
      }, 500);
      this.store.state.blocks.serverVersion =
        this.store.state.blocks.localVersion;
    },
    send: function (blocks) {
      const init = {
        method: "POST",
        body: JSON.stringify(blocks),
      };
      fetch(`./save.json`, init)
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          if (data.status === "error") {
            if (this.$root.debug) {
              console.log(
                "L'enregistrement a échoué. Pour aider à résoudre le problème, envoyez au développeur une capture d'écran complète avec l'erreur suivante : ",
                data
              );
            }
            this.$refs["register-btn"].textContent =
              "Erreur. Ouvrez la console (F12) pour plus d'infos.";
            this.$refs["register-btn"].classList.add("warn");
            setTimeout(() => {
              this.store.state.isUpToDate = true;
            }, 3000);
          } else {
            if (this.$root.debug) {
              console.log(
                "Enregistrement réussi. Nouvelles données de la composition :",
                data
              );
            }
            this.$refs["register-btn"].classList.add("active");
            this.$refs["register-btn"].textContent = "enregistré";
            setTimeout(() => {
              this.store.state.isUpToDate = true;
            }, 1200);
          }
        });
    },
    listenSaveShortcut: function () {
      if (this.$root.debug) {
        console.log(this.$options._componentTag + " : ");
      }
      document.addEventListener(
        "keydown",
        (e) => {
          if (
            (window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) &&
            e.keyCode == 83
          ) {
            e.preventDefault();
            this.save();
          }
        },
        false
      );
    },
  },
  mounted: function () {
    this.listenSaveShortcut();
  },
};

export default Register;
