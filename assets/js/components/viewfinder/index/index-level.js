import Store from "../../../store.js";
import navigateMobileMixin from "../mixins/navigate-mobile-mixin.js";

const IndexLevel = {
  name: "index-level",
  props: {
    "index-level": Array,
    "parent-url": String,
    breadcrumb: Array,
    page: String,
    "parent-name": String,
  },
  mixins: [navigateMobileMixin],
  data: function () {
    return {
      isNextLevelDisplayed: false,
      store: Store,
    };
  },
  computed: {
    isCurrent: function () {
      return this.parentName === this.page;
    },
    containsActiveCategory: function () {
      const containsActiveCategory = this.indexLevel.some((category) => {
        return this.breadcrumb.includes(category.title.toLowerCase());
      });
      return containsActiveCategory;
    },
  },
  template: `
    <ul 
      :class="{
        'lock-visible': containsActiveCategory && store.state.viewfinder.isActive
      }"
    >
      <li v-if="parentUrl.length > 0">
        <span class="wrapper">
          <button 
            class="text-2 border"
            :class="{'lock-active': isCurrent}"
          >
            <a :href="parentUrl">tout</a>
          </button>
        </span>
      </li>
     
      <li
        v-for="(category, childIndex) in indexLevel"
        v-if="store.state.isDesktop || !category.isDesktopOnly"
      >
        <span class="wrapper" :class="isInBreadcrumb(category.title)">
          <button 
            class="text-2 border index__button"
            :class="[isActive(category.url)]"
            tabindex="-1"
          >
            <a 
              :href="category.url"
              :target="defineTarget(category.url)"
              :title="'Aller à la page ' + category.title + '.'"
              class="index__link"
              :class="{ 'index__link--full': category.children === null }"
            >{{ category.title }}</a>
          </button>
          
          <span 
            v-if="category.children"
            class="menu__arrow | text-2 border"
            tabindex="-1"
          ><a 
            @click="navigateTo(category.title, $event)"
            :href="category.url"
            :title="'Aller à la page ' + category.title + '.'"
          >-></a></span>
        </span>
        <index-level
          v-if="category.children"
          :index-level="category.children"
          :parent-url="category.url"
          :parent-name="category.title"
          :breadcrumb="breadcrumb"
          :page="page"
        ></index-level>
      </li>
    </ul>
  `,
  methods: {
    isInBreadcrumb: function (category) {
      if (category.toLowerCase() === "accueil") return false;
      if (this.breadcrumb.includes(category.toLowerCase())) {
        return "lock-active";
      }
    },
    isActive: function (url) {
      if (url + "/" === window.location.href) {
        return "active";
      }
    },
    defineTarget: function (url) {
      console.log(window.location.host);
      if (url.includes(window.location.host)) {
        return false;
      } else {
        return "_blank";
      }
    },
  },
};

export default IndexLevel;
