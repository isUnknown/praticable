import Store from "../../../store.js";
import viewfinderBtnMixin from "../mixins/viewfinder-btn-mixin.js";
import Add from "./add.js";
import navigateMobileMixin from "../mixins/navigate-mobile-mixin.js";

const Modify = {
  mixins: [viewfinderBtnMixin, navigateMobileMixin],
  components: {
    add: Add,
  },
  data: function () {
    return {
      store: Store,
      isOpen: false,
    };
  },
  computed: {
    isModifications: function () {
      const allBackups = JSON.parse(localStorage.getItem("backups"));
      return allBackups[this.store.state.page].length < 1;
    },
  },
  template: `
    <ul class="menu bottom right">
      <li>
        <span @click="toggleIsOpen" :class="{'hidden--mobile': store.state.viewfinder.currentSection.length > 0}">
          <button 
            class="| text-2 border lock-active"
            title="Éditer la page actuelle. Vos modifications sont privées."
          >éditer</button>
          <span
            class="menu__arrow | text-2 border lock-active"
            tabindex="0"
          ><-</span>
        </span>
        <ul v-if="isOpen" class="lock-visible--flex">
          <add
            v-if="store.state.layout !== 'full'"
            @switchOffViewfinder="switchOffViewfinder"
          ></add>
          <li>
            <span>
              <button
                class="text-2 border"
                @click="reset"
                title="Charger la page d'origine. Vos modifications seront supprimées."
              >réinitialiser l'espace</button>
              <!-- <button 
                class="text-2 border"
                @click="reset"
              > ctrl + alt + z</button> -->
            </span>
          </li>
          <li>
            <span>
              <button
                v-if="isModifications"
                class="text-2 border"
                title="Annuler"
                @click="undo"
              >annuler</button>
              <!-- <button 
                class="text-2 border"
                @click="undo"
              > ctrl + z</button> -->
            </span>
          </li>
        </ul>
      </li>
    </ul>
  `,
  methods: {
    switchOffViewfinder: function () {
      this.$emit("switchOffViewfinder");
    },
    reset: function () {
      this.store.reset();
      this.$emit("switchOffViewfinder");
    },
    undo: function () {
      this.store.undo();
      this.$emit("switchOffViewfinder");
    },
  },
};

export default Modify;
