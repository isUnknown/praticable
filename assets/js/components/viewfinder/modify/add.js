import Store from "../../../store.js";

const Add = {
  data: function () {
    return {
      store: Store,
      isOpen: false,
      types: [
        {
          label: "texte",
          type: "markdown",
          open: true,
        },
        {
          label: "image",
          type: "image",
          open: false,
        },
        // {
        // label: "code",
        // type: "code",
        // open: true,
        // },
      ],
    };
  },
  template: `
    <li>
      <span>
        <button 
          class="text-2 border"
          tabindex="-1"
          title="Ajouter un type de bloc à la page actuelle."
        >
          ajouter
        </button>

        <span 
          class="menu__arrow | text-2 border"
          tabindex="-1"
        ><-</span>
      </span>
      <ul>
        <li v-for="item in types" v-if="item.open || store.state.isLog">
          <span>
            <button class="text-2 | border" @click="create(item.type)" :title="'Ajouter un bloc ' + item.label + '.'">{{ item.label }}</button>
          </span>
        </li>
      </ul>
    </li>
  `,
  methods: {
    create: function (type) {
      const newBlockPos = this.setNewBlockPos(type);

      const newBlockConfig = {
        type,
        id: undefined,
        refs: undefined,
        left: newBlockPos.x,
        top: newBlockPos.y,
        text: undefined,
      };

      this.store.addBlock(newBlockConfig);
      this.toggleIsOpen();
      this.$emit("switchOffViewfinder");
    },
    setNewBlockPos: function (type) {
      const newBlockHeight = this.store.state.design.blocks[type].height;
      const halfBlockHeight = newBlockHeight / 2;
      const halfViewHeight = window.innerHeight / 2;

      const newBlockWidth = this.store.state.design.blocks[type].width;
      const halfBlockWidth = newBlockWidth / 2;
      const halfViewWidth = window.innerWidth / 2;

      const XCenterPos = window.scrollX + (halfViewWidth - halfBlockWidth);
      const YCenterPos = window.scrollY + (halfViewHeight - halfBlockHeight);

      const newBlockpos = {
        x: XCenterPos,
        y: YCenterPos,
      };

      return newBlockpos;
    },
    toggleIsOpen: function () {
      this.isOpen = !this.isOpen;
    },
    enableShortcuts: function () {
      document.addEventListener("keydown", (event) => {
        if (event.ctrlKey || event.metaKey) {
          if (event.key === "t") {
            this.create("texte");
          }
        }
      });
    },
  },
};

export default Add;
