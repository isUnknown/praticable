const contextMenuMixin = {
  methods: {
    toggleContext: function (event) {
      if (event.ctrlKey || event.metaKey) {
        event.preventDefault();
        event.stopPropagation();
        const { pageX: mouseX, pageY: mouseY } = event;
        const contextMenu = document.querySelector("#context-menu");
        contextMenu.style.left = `${mouseX - 20}px`;
        contextMenu.style.top = `${mouseY - 20}px`;
        contextMenu.classList.toggle("hidden");

        if (
          event.target.id === "app" ||
          event.target.classList.contains("blocks")
        ) {
          this.store.state.contextMenu.target = {
            type: "viewfinder",
          };
        } else if (event.target.id === "selection") {
          this.store.state.contextMenu.target = {
            type: "selection",
          };
        } else if (this.block) {
          this.store.state.contextMenu.target = this.block;
        }
      }
    },
  },
};

export default contextMenuMixin;
