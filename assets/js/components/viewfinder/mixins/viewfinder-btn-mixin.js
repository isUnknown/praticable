import Store from "../../../store.js";

const viewfinderBtnMixin = {
  data: function () {
    return {
      store: Store,
    };
  },
  computed: {
    isviewfinderActive: function () {
      return this.store.state.viewfinder.isActive;
    },
  },
  watch: {
    isviewfinderActive: function (newValue) {
      console.log("viewfinderChange", newValue);
      if (newValue === false) {
        this.isOpen = false;
      }
    },
  },
  methods: {
    toggleIsOpen: function (menuName) {
      if (this.store.state.viewfinder.isActive) {
        this.$emit("switchOffViewfinder");
      } else {
        this.isOpen = !this.isOpen;
        this.store.state.viewfinder.isActive = this.isOpen;
        if (this.isOpen) {
          this.store.state.viewfinder.currentSection = menuName;
          this.store.state.viewfinder.currentMenu = menuName;

          setTimeout(() => {
            const optionsMaxHeight = this.calculateOptionsMaxHeight();
            if (document.querySelector(".lock-visible") > optionsMaxHeight) {
              document.querySelector(".lock-visible").classList.add("scroll-y");
            }
          }, 100);
        }
      }
    },
    calculateOptionsMaxHeight: function () {
      const btnHeight = document.querySelector(
        "#viewfinder--mobile__current-section"
      ).offsetHeight;
      const optionsMaxHeight = window.innerHeight - btnHeight;

      return optionsMaxHeight;
    },
  },
};

export default viewfinderBtnMixin;
