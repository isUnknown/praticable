import Store from "../../store.js";
import navigateMobileMixin from "./mixins/navigate-mobile-mixin.js";

const CurrentSection = {
  mixins: [navigateMobileMixin],
  data: function () {
    return {
      store: Store,
      previousBtn: false,
    };
  },
  computed: {
    currentSection: function () {
      return this.store.state.viewfinder.currentSection;
    },
  },
  watch: {
    currentSection: function () {
      const activeMenu = document
        .querySelector(".lock-visible")
        .parentNode.querySelector("span > button").textContent;
      this.previousBtn = this.currentSection !== activeMenu;
    },
  },
  template: `
    <div id="viewfinder--mobile__current-section">
      <button @click="previous" class="no-hover" :class="{unvisible: !previousBtn}"><-</button>
      <span id="current-section__title" class="text-2">{{ store.state.viewfinder.currentSection }}</span>
      <button @click="close" class="no-hover"><img id="viewfinder--mobile__current-section__close-image" src="/assets/svg/close.svg" /></button>
    </div>
  `,
  methods: {
    previous: function () {
      const activeSections = document.querySelectorAll(".active-section");
      const currentSectionLi = activeSections[activeSections.length - 1];
      const currentSectionUl = activeSections[activeSections.length - 2];

      currentSectionLi.classList.remove("active-section");
      currentSectionUl.classList.remove("active-section");

      const newActiveSections = document.querySelectorAll(".active-section");

      if (newActiveSections.length === 0) {
        this.store.state.viewfinder.currentSection =
          this.store.state.viewfinder.currentMenu;
      } else {
        this.store.state.viewfinder.currentSection =
          newActiveSections[newActiveSections.length - 1].querySelector(
            "span > button"
          ).textContent;
      }
      this.sizeDisplayedUl();
    },
    close: function () {
      this.$emit("off");
    },
  },
};

export default CurrentSection;
