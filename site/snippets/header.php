<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <?php snippet('meta') ?>
    <link rel="icon" type="image/x-icon" href="<?= url('assets') ?>/images/icons/favicon.svg">
    <!-- à remplacer quand le site sera en prod par "index,follow" -->
    <meta name="robots" content="noindex, nofollow, noarchive"> 
    <link rel="canonical" href="<?= $page->url() ?>">
    
    <!--========== INTERACT ==========-->
    <script src="<?= url('assets') ?>/js/libs/interact.min.js"></script>
    <script src="<?= url('assets') ?>/js/libs/vue-interactjs.umd.js"></script>
    
    <!--========== VUE ==========-->
    <script src="<?= url('assets') ?>/js/libs/vue.js"></script>

    <!--========== DEVELOPMENT : RAW ==========-->
    <script src="<?= url('assets') ?>/js/app.js" type="module" defer></script>
    <link rel="stylesheet" href="<?= url('assets') ?>/css/style.css?version-cache-prevent<?= rand(0, 1000)?>">

    <!--========== PRODUCTION : BUNDLES ==========-->
    <!-- <script src="<?= url('assets') ?>/dist/app.bundle.js" defer></script> -->
    <!-- <link rel="stylesheet" href="<?= url('assets') ?>/dist/style.css?version-cache-prevent<?= rand(0, 1000)?>" -->
    
    <!--========== MARKEDJS (MARKDOWN PARSER) ==========-->
    <script src="<?= url('assets') ?>/js/libs/marked.min.js"></script>

    <!--========== FONTS ==========-->
    <!-- Fira code -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraCode-Light.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraCode-Regular.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraCode-Medium.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraCode-SemiBold.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraCode-Bold.woff2" type="font/woff2" crossorigin="anonymous">

    <!-- Fira sans -->
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Bold.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-BoldItalic.woff2" type="font/woff2" crossorigin="anonymous">
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Book.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-BookItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Eight.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-EightItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-ExtraBold.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-ExtraBoldItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Light.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-LightItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-ExtraLight.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-ExtraLightItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Four.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-FourItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Hair.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-HairItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Heavy.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-HeavyItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Italic.woff2" type="font/woff2" crossorigin="anonymous">
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Light.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-LightItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Medium.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-MediumItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Regular.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-SemiBold.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-SemiBoldItalic.woff2" type="font/woff2" crossorigin="anonymous">
    <!-- <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Thin.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-ThinItalic.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Two.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-TwoItalic.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-Ultra.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-UltraItalic.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-UltraLight.woff2" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/FiraSans-UltraLightItalic.woff2" type="font/woff2" crossorigin="anonymous"> -->

    <!--========== PRELOAD ==========-->
    <link rel="preload" as="image" href="<?= url('assets') ?>/images/icons/facebook-hover.svg">
    <link rel="preload" as="image" href="<?= url('assets') ?>/images/icons/twitter-hover.svg">
    <link rel="preload" as="image" href="<?= url('assets') ?>/images/icons/instagram-hover.svg">
    <link rel="preload" as="image" href="<?= url('assets') ?>/images/icons/mastodon-hover.svg">
    <link rel="preload" as="image" href="<?= url('assets') ?>/images/icons/linkedin-hover.svg">
</head>
<body>
    <?php snippet('svg') ?>