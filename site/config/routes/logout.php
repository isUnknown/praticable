<?php

return [
  'pattern' => '(:all)logout.php',
  'action'  => function () {
    $kirby = kirby();
    $user = $kirby->user();
    $user->logout();
    session_start();
    go($_SESSION["redirect_url"]);
    return '<html><body>logout</body></html>';
  }
];