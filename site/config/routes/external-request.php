<?php

return [
  "pattern" => ['(:any)/external-request.json', '/external-request.json'],
  "method" => "POST",
  "action" => function() {
    $json = file_get_contents("php://input");
    $data = json_decode($json);

    $id = $data->id;

    $site = site();
    $page = $site->index()->findBy('distantId', $id);

    $url = 'https://api.notion.com/v1/blocks/' . $id . '/children?page_size=100';

    $headers = [
      'Notion-Version: 2022-02-22',
      'Authorization: Bearer secret_MmP0JeLbNjw9fSCx1KIjOrYPsRm03bpRoS5xxWqXXWP'
    ];

    //QUERY API
    $curl = curl_init();
    $json = getNotionBlocks($curl, $url, $headers);
    curl_close($curl);

    $api = $site->apis()->toStructure()->findBy('name', 'Notion');
    // createJSONFile($page, (string)$api->id(), $json);

    $response = json_decode($json);
    $incomingBlocks = $response->results;
    
    $preparedBlocks = prepareBlocksRecursively($incomingBlocks, $api, $page, $id);

    $kirby = kirby();
    $kirby->impersonate('kirby');

    $page->update([
      'composition' => json_encode($preparedBlocks)
    ]);

    return json_encode($preparedBlocks);

  }
];