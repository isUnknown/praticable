<?php

$kirby = kirby();
$target = $kirby->root('assets') . '/static-data/' . $page->slug();

if (is_dir($target)) {
  deleteAll($target);
}