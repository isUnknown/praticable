<?php

return [
    'debug' => true,
    'thumbs' => [
      'presets' => [
        'default' => [
          'width' => 1920
        ]
      ]
    ],
    'smartypants' => true,
    'routes' => [
        require_once 'routes/save.php',
        require_once 'routes/logout.php',
        require_once 'routes/fetch.php',
        require_once 'routes/external-request.php',
        require_once 'routes/sitemap-xml.php',
        require_once 'routes/sitemap.php'
    ],
    'hooks' => [
      'page.create:after' => function ($page) {
        require_once 'hooks/create-static-dir.php';
        // mkdir($page->root() . '/json');
      },
      'page.update:after' => function ($oldPage, $newPage) {
        require_once 'hooks/create-or-update-representatives.php';
      },
      'page.changeStatus:after' => function (Kirby\Cms\Page $newPage, Kirby\Cms\Page $oldPage) {
        if ($newPage->isPublished()) {
          require_once 'hooks/create-or-update-representatives.php';
        } else if ($newPage->isDraft()) {
          require_once 'hooks/remove-parent-representative.php';
        }
      },
      'page.delete:before' => function ($page) {
        require_once 'hooks/remove-static-dir.php';
        require_once 'hooks/remove-parent-representative.php';
      },
      'file.create:after' => function ($file) {
        // GET GLOBAL OBJECTS
        $kirby = kirby();
        $site = site();
        $page = $file->page();
  
        if ($file->type() == 'archive') {
          require_once 'hooks/create-html-from-archive.php';
        }
      },
      'file.delete:before' => function ($file) {
        if ($file->type() == 'archive') {
          require_once 'hooks/remove-html-from-archive.php';
        }
      }
    ]
];