panel.plugin('adrienpayet/fetchAPI', {
  fields: {
    fetchAPI: {
      data() {
        return {
          title: this.$store.getters['content/id']().split('/').pop()
        }
      },
      template: `
        <k-button icon="refresh" @click="refresh">Récupérer les données</k-button>
      `,
      methods: {
        refresh() {
          const myHeaders = new Headers();
          const myBody = JSON.stringify({
            'pageTitle': this.title
          })

          const myInit = { 
            method: 'POST',
            headers: myHeaders,
            body: myBody
          }

          console.log(myInit)

          const icon = document.querySelector('[type=fetchAPI] .k-icon-refresh')
          const text = document.querySelector('[type=fetchAPI] .k-button-text')

          icon.classList.add('spin')
          text.innerHTML = text.innerHTML + '<br /><strong>Ne pas rafraîchir ou fermer la page.</strong> Cette opération peut durer plusieurs minutes. Le site récupère les données depuis des serveurs distants et les installe.'

          fetch('/praticable/fetch.json', myInit)
            .then(res => {
              return res.json()
            })
            .then(json => {
              console.log(json)
              location.reload()
            })
        }
      }
    }
  }
});