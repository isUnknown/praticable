<?php

function createRepresentativeBlock($representedPage, $linkedSpaces, $existingRepresentative = null) {
  $blocks = $representedPage->composition()->toBlocks();
  $cover = $blocks->filterBy('iscover', true)->first();
  $intro = $blocks->filterBy('isintro', true)->first();

  $preparedBlock = new Kirby\Cms\Block([
    'content' => [
        'title' => $representedPage->title()->value(),
        'cover' => $cover == null ? false : $cover->image()->toFile()->url(),
        'intro' => $intro == null ? false : $intro->text()->value(),
        'width' => $existingRepresentative !== null ? $existingRepresentative->width()->value() : '400px',
        'height' => $existingRepresentative !== null ? $existingRepresentative->height()->value(): '',
        'transform' => $existingRepresentative !== null ? $existingRepresentative->transform()->value() : '',
        'zindex' => $existingRepresentative !== null ? $existingRepresentative->zindex()->value() : 0,
        'index' => $representedPage->index()->value(),
        'url' => $representedPage->url(), 
        'tags' => $representedPage->tags()->split(),
        'linkedSpaces' => $linkedSpaces
    ],
    'id' => $representedPage->id(),
    'type' => 'representative'
  ]);

  return $preparedBlock;
}

function addToBlocksAtIndex($blocks, $block, $index) {    
  $arrayBlocks = $blocks->toArray();
  $arrayBlock = [$block->toArray()];

  array_splice($arrayBlocks, $index, 1, $arrayBlock);
  
  $newBlocks = Kirby\Cms\Blocks::factory($arrayBlocks);
  return $newBlocks;
}
function addRepresentativeToBlocks($params) {
  $blocks = $params['blocks'];
  $representedPage = $params['representedPage'];
  $existingRepresentative = $params['existingRepresentative'];
  $linkedSpaces = $params['linkedSpaces'];
  
  $preparedBlock = createRepresentativeBlock($representedPage, $linkedSpaces, $existingRepresentative);

  if ($existingRepresentative != null) {
    $existingIndex = $existingRepresentative->indexOf();
    $newBlocks = addToBlocksAtIndex($blocks, $preparedBlock, $existingIndex);  
  
  } else {
    $blocks = $blocks->remove($representedPage->id());
    $newBlocks = $blocks->add($preparedBlock);
  }
  return $newBlocks;
}